import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:zebrapro_announcements/data/AppDataManager.dart';
import 'package:zebrapro_announcements/data/network/NetworkExceptions/NetworkExceptions.dart';
import 'package:zebrapro_announcements/data/network/model/ApiResult/ApiResult.dart';
import 'package:zebrapro_announcements/data/network/model/ChannelListResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ChannelSubscriptionListResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/VerifyTokenResponse.dart';
import 'package:zebrapro_announcements/services/AppFirebaseMessagingServices.dart';
import 'package:zebrapro_announcements/utils/enums/ViewState.dart';
import 'package:zebrapro_announcements/utils/enums/bottomSheetEnum.dart';

class BaseViewModel extends ChangeNotifier {
  final AppDataManager _dataManager = Get.find<AppDataManager>();

  final BottomSheetService _bottomSheetService = Get.find<BottomSheetService>();

  final DialogService _dialogService = Get.find<DialogService>();

  AppDataManager getDataManager() => _dataManager;

  DialogService getDialogService() => _dialogService;

  BottomSheetService getBottomSheetService() => _bottomSheetService;

  ViewState _state = ViewState.idle;

  ViewState get state => _state;

  void setState(ViewState viewState, {bool shouldUpdate = true}) {
    if (shouldUpdate) {
      _state = viewState;
      notifyListeners();
    }
  }

  refreshScreen() {
    notifyListeners();
  }

  showProgressBar({String title = "Please wait..."}) {
    EasyLoading.show(status: title);
  }

  stopProgressBar() {
    EasyLoading.dismiss();
  }

  @override
  void dispose() {}

  showErrorDialog({String title = "Problem occurred",
    String description = "Some problem occurred Please try again",
    bool isDismissible = true}) async {
    await _bottomSheetService.showCustomSheet(
        title: title,
        description: description,
        mainButtonTitle: "OK",
        variant: BottomSheetEnum.error,
        barrierDismissible: isDismissible);
  }

  showNoInternetAlert({String title = "PhoneAuthHolderViewModelNet Connection",
    String description = "Please Connect to Stable Network and Try Again",
    bool isdismmisible = false,
    mainButtonTitle = "RETRY"}) async {
    await _bottomSheetService.showCustomSheet(
        title: title,
        description: description,
        mainButtonTitle: mainButtonTitle,
        variant: BottomSheetEnum.noInternet,
        barrierDismissible: isdismmisible);
  }

  runStartupLogic() async {
    String? token = await getDataManager().getServerAuthToken();
    // Firebase
    await AppFirebaseMessagingServices().setupFirebaseNotifications();

    if (token != null) {
      await verifyToken();
      await FirebaseAnalytics.instance.logAppOpen();
      await FirebaseAnalytics.instance.logEvent(name: "AppOpen", parameters: {
        "cust_id": getDataManager().getId(),
        "cust_inf": getDataManager().getUsername()
      });
      gotoHome();
    } else {
      gotoLogin();
    }
  }

  Future<void> setupNotificationChannels() async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();

    var initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');

    ApiResult<ChannelSubscriptionListResponseModel> result =
        await getDataManager().getChannelSubscriptionsApiCall();
    result.when(success: (ChannelSubscriptionListResponseModel model) async {
      setState(ViewState.idle);
      if (model.success! &&
          model.data != null &&
          model.data!.myChannelsSub != null) {
        for (MyChannelsSub channel in model.data!.myChannelsSub!) {
          if (!channel.isSubscribed!) {
            var androidNotificationChannel = AndroidNotificationChannel(
              channel.channelSlug!,
              channel.channelName!,
            );
            await flutterLocalNotificationsPlugin
                .resolvePlatformSpecificImplementation<
                    AndroidFlutterLocalNotificationsPlugin>()
                ?.createNotificationChannel(androidNotificationChannel);
          }
        }
      }
    }, failure: (NetworkExceptions error) {
      setState(ViewState.idle);
      handleGeneralApiError(error, () {});
    });
  }

  void gotoHome() {
    Get.offNamedUntil('/home', (route) => false);
  }

  void gotoLogin() {
    Get.offNamedUntil('/login', (route) => false);
  }

  Future handleGeneralApiError(NetworkExceptions error, Function callback,
      {String title = "Problem occurred",
        String description = "Some problem occurred Please try again",
        bool isDismissible = true,
        String mainButtonTitle = "RETRY"}) async {
    bool handled = await handleApiError(error, () async {
      callback();
    }, isDismissible, mainButtonTitle);
    print(error);

    String errorMessage = NetworkExceptions.getErrorMessage(error);

    if (!handled) {
      showErrorDialog(
          title: title,
          description: errorMessage,
          isDismissible: isDismissible);
    }
  }

  Future<bool> handleApiError(NetworkExceptions error, Function callback,
      [bool isDismissible = false, String mainButtonTitle = "RETRY"]) async {
    if (error == NetworkExceptions.noInternetConnection()) {
      await showNoInternetAlert(
          title: "No Internet Connection",
          description: "Please make sure that you are connected to internet",
          isdismmisible: isDismissible,
          mainButtonTitle: mainButtonTitle);
      callback();
      return true;
    }
    if (error == NetworkExceptions.unauthorisedRequest()) {
      await showErrorDialog(
          title: "Unauthenticated",
          description: "Logged in from another device, Please Login Again!",
          isDismissible: isDismissible);
      getDataManager().clearAllPreferences();
      gotoLogin();
      callback();
      return true;
    } else {
      return false;
    }
  }

  verifyToken() async {
    ApiResult<VerifyTokenResponse> result =
    await getDataManager().verifyTokenApiCall();
    result.when(success: (VerifyTokenResponse model) async {
      setState(ViewState.idle);
      if (model.data != null && model.data?.id != null) {
        getDataManager().setId(model.data!.id!);
        getDataManager().setRoleId(model.data!.roleId!);
        getDataManager().setName(model.data!.name!);
        getDataManager().setUsername(model.data!.username!);
        getDataManager().setEmail(model.data!.email!);
        getDataManager().setPhone(model.data!.phone!);
        getDataManager().setIsActive(model.data!.isActive!);
        getDataManager().setCreatedAt(model.data!.createdAt!);
        getDataManager().setPmsCompanyId(model.data!.pmsCompanyId!);
      }
    }, failure: (NetworkExceptions error) {
      setState(ViewState.idle);
      handleGeneralApiError(error, () {});
    });
  }
}
