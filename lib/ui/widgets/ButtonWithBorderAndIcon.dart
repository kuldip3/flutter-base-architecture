import 'package:flutter/material.dart';

class ButtonWithBorderAndIcon extends StatelessWidget {
  final IconData icon;
  final String label;

  ButtonWithBorderAndIcon({
    required this.icon,
    required this.label,
  });

  @override
  Widget build(BuildContext context) {
    return TextButton.icon(
      onPressed: () {},
      icon: Icon(icon),
      label: Text(label),
      style: TextButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
      ),
    );
  }
}
