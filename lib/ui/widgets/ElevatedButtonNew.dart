import 'package:flutter/material.dart';

class ElevatedButtonNew extends ElevatedButton {
  double nRadius;

  ElevatedButtonNew({
    this.nRadius = 12,
    required VoidCallback? onPressed,
    required Widget? child,
  }) : super(
            onPressed: onPressed,
            child: child,
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(nRadius),
            )));
}
