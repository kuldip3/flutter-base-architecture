import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zebrapro_announcements/config/ColorConfig.dart';
import 'package:zebrapro_announcements/ui/base/BaseView.dart';
import 'package:zebrapro_announcements/ui/screens/settings/SettingsView.dart';
import 'package:zebrapro_announcements/ui/widgets/nav_drawer/NavDrawerViewModel.dart';
import 'package:zebrapro_announcements/utils/ViewUtils.dart';

class NavDrawer extends StatefulWidget {
  Function(int screenIndex) changeMenu;
  Function(bool toggle) toggleMenu;

  NavDrawer({required this.changeMenu, Key? key, required this.toggleMenu})
      : super(key: key);

  @override
  State<NavDrawer> createState() => _NavDrawerState();
}

class _NavDrawerState extends State<NavDrawer> {
  String name = "Hello, ";

  @override
  Widget build(BuildContext context) {
    return BaseView<NavDrawerViewModel>(
      onModelReady: (model) {
        name += "${model.getDataManager().getName()}";
      },
      builder: (BuildContext context, model, Widget? child) => Padding(
        padding: const EdgeInsets.only(top: 52, bottom: 86.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                widget.changeMenu(0);
                widget.toggleMenu(false);
              },
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 16, right: 16, top: 56, bottom: 11),
                child: Row(
                  children: [
                    SvgPicture.asset(
                      "assets/svg/megaphone-fill.svg",
                      semanticsLabel: 'Announcements',
                      height: 18,
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    Text(
                      "Announcements",
                      style: TextStyle(
                        color: ColorConfig.primaryTextColor,
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                widget.changeMenu(1);
                widget.toggleMenu(false);
              },
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 11),
                child: Row(
                  children: [
                    SvgPicture.asset(
                      "assets/svg/file-earmark-medical-fill.svg",
                      semanticsLabel: 'Report Search',
                      height: 18,
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    Text(
                      "Report Search",
                      style: TextStyle(
                        color: ColorConfig.primaryTextColor,
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                widget.changeMenu(2);
                widget.toggleMenu(false);
              },
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 11),
                child: Row(
                  children: [
                    SvgPicture.asset(
                      "assets/svg/stat-scanner.svg",
                      semanticsLabel: 'Stat Scanner',
                      height: 18,
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    Text(
                      "Stat Scanner",
                      style: TextStyle(
                        color: ColorConfig.primaryTextColor,
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
            ),
            const Spacer(),
            InkWell(
              onTap: () {
                widget.changeMenu(3);
                widget.toggleMenu(false);
              },
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 11),
                child: Row(
                  children: [
                    SvgPicture.asset(
                      "assets/svg/person.svg",
                      semanticsLabel: 'Profile',
                      height: 18,
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    Text(
                      "Profile",
                      style: TextStyle(
                        color: ColorConfig.primaryTextColor,
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () {
                Get.to(() => const SettingsView(),
                    transition: Transition.rightToLeft);
                widget.toggleMenu(false);
              },
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 11),
                child: Row(
                  children: [
                    SvgPicture.asset(
                      "assets/svg/settings.svg",
                      semanticsLabel: 'Settings',
                      height: 18,
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    Text(
                      "Settings",
                      style: TextStyle(
                        color: ColorConfig.primaryTextColor,
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () async {
                if (await launchUrl(Uri.parse("https://zebrapro.in/contact"))) {
                  Get.snackbar("Something went wrong", "Url can't open",
                      backgroundColor: ColorConfig.red);
                }
              },
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 11),
                child: Row(
                  children: [
                    SvgPicture.asset(
                      "assets/svg/contacts.svg",
                      semanticsLabel: 'Contacts',
                      height: 18,
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    Text(
                      "Contact Us",
                      style: TextStyle(
                        color: ColorConfig.primaryTextColor,
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
              child: SizedBox(
                width: ViewUtils.getWidthPercent(context, 38),
                child: OutlinedButton(
                  onPressed: () {
                    model.getDataManager().clearAllPreferences();
                  },
                  child: const Text("Sign Out"),
                  style: OutlinedButton.styleFrom(
                      backgroundColor: ColorConfig.red,
                      side: BorderSide(color: ColorConfig.red)),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
