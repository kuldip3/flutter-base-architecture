import 'package:zebrapro_announcements/data/network/NetworkExceptions/NetworkExceptions.dart';
import 'package:zebrapro_announcements/data/network/model/ApiResult/ApiResult.dart';
import 'package:zebrapro_announcements/data/network/model/BookmarkSaveResponse.dart';
import 'package:zebrapro_announcements/data/network/model/ReportSearchResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ReportSearchResultLikeResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/requests/BookmarkRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/ReportSearchRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/ReportSearchResultLikeRequest.dart';
import 'package:zebrapro_announcements/ui/base/BaseViewModel.dart';
import 'package:zebrapro_announcements/utils/enums/ViewState.dart';

class ReportSearchResultViewModel extends BaseViewModel {
  String table_name = "search_engine_resource_images";

  Future<ReportSearchResponseModel?>? searchReports(
      String keyword, String tags) async {
    ReportSearchResponseModel? responseModel;
    setState(ViewState.busy);
    ReportSearchRequest request =
        ReportSearchRequest(keyword: keyword, preferencelist: tags);

    ApiResult<ReportSearchResponseModel> result =
        await getDataManager().getReportSearchApiCall(request);
    responseModel =
    await result.when(success: (ReportSearchResponseModel model) async {
      setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      handleGeneralApiError(error, () {});
      setState(ViewState.idle);
      return null;
    });

    return responseModel;
  }

  Future<ReportSearchResultLikeResponseModel?>? postReactionOnSearchResult(int resultId, int reaction) async {
    ReportSearchResultLikeResponseModel? responseModel;
    // setState(ViewState.busy);
    ReportSearchResultLikeRequest request =
    ReportSearchResultLikeRequest(itemId: resultId, like: reaction);

    ApiResult<ReportSearchResultLikeResponseModel> result =
    await getDataManager().postLikeToReportSearchResultApiCall(request);
    responseModel = await result.when(
        success: (ReportSearchResultLikeResponseModel model) async {
          // setState(ViewState.idle);
          return model;
        }, failure: (NetworkExceptions error) {
      // setState(ViewState.idle);
      handleGeneralApiError(error, () {});
      return null;
    });

    return responseModel;
  }

  Future<BookmarkSaveResponse?>? postBookmarkSearchResult(
      {required int resultId, required String tableName}) async {
    BookmarkSaveResponse? responseModel;
    // setState(ViewState.busy);
    BookmarkRequest request =
        BookmarkRequest(postId: resultId, tableName: tableName);

    ApiResult<BookmarkSaveResponse> result =
        await getDataManager().postBookmarkItemApiCall(request);
    responseModel =
        await result.when(success: (BookmarkSaveResponse model) async {
      // setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      // setState(ViewState.idle);
      handleGeneralApiError(error, () {});
      return null;
    });

    return responseModel;
  }

  Future<ReportSearchResponseModel?>? getProcessingStatus(String keyword, String tags) async {
    ReportSearchResponseModel? responseModel;
    setState(ViewState.busy);
    ReportSearchRequest request =
    ReportSearchRequest(keyword: keyword, preferencelist: tags);

    ApiResult<ReportSearchResponseModel> result =
    await getDataManager().getReportSearchApiCall(request);
    responseModel =
    await result.when(success: (ReportSearchResponseModel model) async {
      setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      handleGeneralApiError(error, () {});
      setState(ViewState.idle);
      return null;
    });

    return responseModel;
  }
}
