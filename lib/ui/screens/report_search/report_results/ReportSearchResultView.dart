import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:zebrapro_announcements/config/ApiConfig.dart';
import 'package:zebrapro_announcements/config/ColorConfig.dart';
import 'package:zebrapro_announcements/data/network/model/BookmarkSaveResponse.dart';
import 'package:zebrapro_announcements/data/network/model/ReportSearchResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ReportSearchResultLikeResponseModel.dart';
import 'package:zebrapro_announcements/ui/base/BaseView.dart';
import 'package:zebrapro_announcements/ui/widgets/AppPdfViewer.dart';
import 'package:zebrapro_announcements/utils/ViewUtils.dart';
import 'package:zebrapro_announcements/utils/enums/ViewState.dart';

import 'ReportSearchViewModel.dart';

class ReportSearchResultView extends StatefulWidget {
  const ReportSearchResultView({Key? key}) : super(key: key);

  @override
  State<ReportSearchResultView> createState() => _HomeViewState();
}

class _HomeViewState extends State<ReportSearchResultView> {
  List<String> argTags = Get.arguments['tags'];
  String argQuery = Get.arguments['query'];
  List<Resp>? results;
  List<Resp1>? preferredResults;
  bool _showToolbar = false;
  bool _dataLoaded = false;
  ReportSearchResultViewModel? _model;
  final TextEditingController _editingController = TextEditingController();
  bool _isSearchInitiated = false;
  FocusNode? focusNode;

  @override
  Widget build(BuildContext context) {
    return BaseView<ReportSearchResultViewModel>(onModelReady: (model) async {
      _model = model;
      await fetchResults(argQuery, argTags.join('|').toLowerCase());
    }, builder: (BuildContext context, model, Widget? child) {
      if (model.state == ViewState.busy) {
        EasyLoading.show(status: 'Please wait...');
      } else {
        EasyLoading.dismiss();
        if (results == null) {
          return const Center(
            child: Text("Sorry No Content Found"),
          );
        }
      }
      return Scaffold(
        appBar: _showToolbar
            ? AppBar(
                flexibleSpace: SafeArea(
                  child: Row(
                    children: [
                      Material(
                        color: Colors.transparent,
                        child: IconButton(
                          icon: Icon(
                            Icons.arrow_back,
                            color: Color(0x00000000).withOpacity(0.54),
                            size: 24,
                          ),
                          onPressed: () {
                            _isSearchInitiated = false;
                            _editingController.clear();
                            setState(() {
                              _showToolbar = false;
                            });
                          },
                        ),
                      ),
                      Flexible(
                        child: TextFormField(
                          style: TextStyle(
                              color: Color(0x00000000).withOpacity(0.87),
                              fontSize: 16),
                          enableInteractiveSelection: false,
                          focusNode: focusNode,
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.search,
                          controller: _editingController,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Find...',
                            hintStyle: TextStyle(
                                color: Color(0x00000000).withOpacity(0.34)),
                          ),
                          onChanged: (text) {
                            if (_editingController.text.isNotEmpty) {
                              setState(() {});
                            }
                          },
                          onFieldSubmitted: (String value) async {
                            if (kIsWeb) {
                              // _pdfTextSearchResult =
                              //     widget.controller!.searchText(_editingController.text);
                              // if (_pdfTextSearchResult.totalInstanceCount == 0) {
                              //   widget.onTap?.call('noResultFound');
                              // }
                              setState(() {});
                            } else {
                              _isSearchInitiated = true;
                              setState(() {
                                argQuery = _editingController.text;
                              });
                              await fetchResults(_editingController.text,
                                  argTags.join("|").toLowerCase());
                            }
                          },
                        ),
                      ),
                      Visibility(
                        visible: _editingController.text.isNotEmpty,
                        child: Material(
                          color: Colors.transparent,
                          child: IconButton(
                            icon: const Icon(
                              Icons.clear,
                              color: Color.fromRGBO(0, 0, 0, 0.54),
                              size: 24,
                            ),
                            onPressed: () {
                              setState(() {
                                _editingController.clear();
                                _isSearchInitiated = false;
                                focusNode!.requestFocus();
                              });
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                automaticallyImplyLeading: false,
                backgroundColor: const Color(0xFFFAFAFA),
              )
            : AppBar(
                title: Text(argQuery),
                titleTextStyle: TextStyle(
                    color: ColorConfig.primaryTextColor, fontSize: 22),
                iconTheme: IconThemeData(color: ColorConfig.primaryTextColor),
                elevation: 6,
                shadowColor: const Color(0x1A000000),
                actions: [
                  IconButton(
                    onPressed: () {
                      setState(() {
                        _showToolbar = true;
                      });
                    },
                    icon: const Icon(Icons.search),
                  )
                ],
              ),
        body: _dataLoaded
            ? Padding(
                padding: const EdgeInsets.all(16.0),
                child: CustomScrollView(
                  physics: const BouncingScrollPhysics(),
                  slivers: [
                    preferredResults != null
                        ? SliverGrid(
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                  crossAxisSpacing: 4,
                  mainAxisSpacing: 1,
                  childAspectRatio: 0.7,
                ),
                delegate: SliverChildBuilderDelegate(
                        (context, index) => InkWell(
                      onTap: () {
                                        Map<String, dynamic> result =
                                            preferredResults![index].toJson();
                                        viewResultDetails(context, result);
                                      },
                      child: Card(
                        clipBehavior: Clip.hardEdge,
                        borderOnForeground: true,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.circular(6)),
                        elevation: 6,
                        child: Banner(
                          location: BannerLocation.topStart,
                          message: "PREFERRED",
                          color: Colors.red,
                          child: Image(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                                ApiConfig.SECURE_IMAGE_URL +
                                    preferredResults![index]
                                        .thumbnail!),
                          ),
                        ),
                      ),
                    ),
                    childCount: preferredResults!.length),
              )
                  : const SliverList(
                delegate: SliverChildListDelegate.fixed([
                  Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Text(
                      "No Preferred Results",
                      style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold),
                    ),
                  )
                ]),
              ),
              results != null
                  ? SliverGrid(
                gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              crossAxisSpacing: 4,
                              mainAxisSpacing: 1,
                              childAspectRatio: 0.7,
                            ),
                            delegate: SliverChildBuilderDelegate(
                                (context, index) => InkWell(
                                      onTap: () {
                                        Map<String, dynamic> result =
                                            results![index].toJson();
                                        viewResultDetails(context, result);
                                      },
                                      child: Card(
                                        clipBehavior: Clip.hardEdge,
                                        borderOnForeground: true,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(6)),
                                        elevation: 6,
                                        child: Image(
                                          fit: BoxFit.cover,
                                          image: NetworkImage(
                                              ApiConfig.SECURE_IMAGE_URL +
                                                  results![index].thumbnail!),
                                        ),
                                      ),
                                    ),
                                childCount: results!.length),
                          )
                  : const SliverList(
                delegate: SliverChildListDelegate.fixed(
                                [Text("No Preferred Results")]),
                          ),
                  ],
                ),
              )
            : Container(),
      );
    });
  }

  Future<void> fetchResults(String query, String tags) async {
    ReportSearchResponseModel? response =
        await _model!.searchReports(query, tags);
    setState(() {
      if (response != null && response.data != null) {
        _dataLoaded = response.success!;
        if (response.data!.resp != null) {
          results = response.data!.resp;
        }
        if (response.data!.resp1 != null) {
          preferredResults = response.data!.resp1;
        }
      }
    });
  }

  void viewResultDetails(context, Map<String, dynamic> result) {
    showBottomSheet(
        context: context,
        clipBehavior: Clip.hardEdge,
        elevation: 12,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(22), topLeft: Radius.circular(22))),
        builder: (context) {
          return buildBottomDialog(result);
        });
  }

  StatefulBuilder buildBottomDialog(Map<String, dynamic> result) {
    int totalLikes = result['total_likes'];
    int liked = result['like'];
    int totalDislikes = result['total_dislikes'];
    int disliked = result['dislike'];
    int saved = result['save'];

    return StatefulBuilder(
      builder: (context, setState) {
        return Container(
          color: ColorConfig.black,
          height: ViewUtils.getHeightPercent(context, 50),
          padding: const EdgeInsets.all(16),
          child: GridView(
            physics: const NeverScrollableScrollPhysics(),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 4,
              mainAxisSpacing: 1,
              childAspectRatio: 0.2,
            ),
            children: [
              Column(
                children: [
                  Card(
                    clipBehavior: Clip.hardEdge,
                    borderOnForeground: true,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6)),
                    elevation: 6,
                    child: Image(
                      fit: BoxFit.cover,
                      height: ViewUtils.getHeightPercent(context, 28),
                      image: NetworkImage(
                          ApiConfig.SECURE_IMAGE_URL + result['thumbnail']!),
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: InkWell(
                            onTap: () async {
                              BookmarkSaveResponse? bookmarked = await _model!
                                  .postBookmarkSearchResult(
                                      resultId: result['id'],
                                      tableName: 'resource_finder');
                              if (bookmarked != null &&
                                  bookmarked.data != null) {
                                setState(() {
                                  saved = bookmarked.data!.favourite! ? 1 : 0;
                                });
                              }
                            },
                            child: Container(
                              height: 34,
                              decoration: BoxDecoration(
                                  color:
                                      saved == 1 ? Colors.white : Colors.black,
                                  borderRadius: BorderRadius.circular(6),
                                  border: Border.all(
                                      color: Colors.white, width: 1)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(
                                    "assets/svg/add_bookmark.svg",
                                    height: 18,
                                    color: saved == 1
                                        ? Colors.black
                                        : Colors.white,
                                  ),
                                  const SizedBox(
                                    width: 6,
                                  ),
                                  Text(
                                    "Save",
                                    style: TextStyle(
                                      color: saved == 1
                                          ? Colors.black
                                          : Colors.white,
                                      fontSize: 12,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: InkWell(
                            onTap: () {
                              Get.to(() => AppPdfViewer(
                                    pdfUrl: result['resource_link']!,
                                    title: result['keyword']!,
                                  ));
                            },
                            child: Container(
                              height: 34,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(6),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: const [
                                  Text(
                                    "View",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 12),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: InkWell(
                            onTap: () async {
                              ReportSearchResultLikeResponseModel?
                                  likeResponse =
                                  await _model?.postReactionOnSearchResult(
                                      result['id']!, 1);
                              if (likeResponse != null &&
                                  likeResponse.data != null) {
                                setState(() {
                                  totalLikes = likeResponse.data!.likesCount!;
                                  totalDislikes =
                                      likeResponse.data!.dislikesCount!;
                                  liked = likeResponse.data!.like! == 1 ? 1 : 0;
                                  disliked =
                                      likeResponse.data!.like! == 2 ? 1 : 0;
                                });
                              }
                            },
                            child: SizedBox(
                              height: 42,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(
                                    "assets/svg/like_thumb.svg",
                                    height: 18,
                                    color: liked > 0
                                        ? Colors.white
                                        : Colors.grey.shade600,
                                  ),
                                  const SizedBox(
                                    width: 6,
                                  ),
                                  Text(
                                    totalLikes.toString(),
                                    style: const TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: InkWell(
                            onTap: () async {
                              ReportSearchResultLikeResponseModel?
                                  likeResponse =
                                  await _model?.postReactionOnSearchResult(
                                      result['id']!, 2);
                              if (likeResponse != null &&
                                  likeResponse.data != null) {
                                setState(() {
                                  totalLikes = likeResponse.data!.likesCount!;
                                  totalDislikes =
                                      likeResponse.data!.dislikesCount!;
                                  liked = likeResponse.data!.like! == 1 ? 1 : 0;
                                  disliked =
                                      likeResponse.data!.like! == 2 ? 1 : 0;
                                });
                              }
                            },
                            child: SizedBox(
                              height: 42,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(
                                    "assets/svg/dislike_thumb.svg",
                                    height: 18,
                                    color: disliked > 0
                                        ? Colors.white
                                        : Colors.grey.shade600,
                                  ),
                                  const SizedBox(
                                    width: 6,
                                  ),
                                  Text(
                                    totalDislikes.toString(),
                                    style: const TextStyle(color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        result['pdf_created_at']!,
                        softWrap: true,
                        style:
                            const TextStyle(fontSize: 10, color: Colors.white),
                      ),
                      const Spacer(),
                      InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: const Icon(
                            Icons.clear,
                            color: Colors.white,
                            size: 18,
                          ))
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Text(
                    result['title']!,
                    softWrap: true,
                    style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: Colors.white),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Text(
                    result['snippet']!,
                    softWrap: true,
                    style: TextStyle(fontSize: 12, color: Colors.grey.shade400),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
