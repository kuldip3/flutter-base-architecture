import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:zebrapro_announcements/config/ColorConfig.dart';
import 'package:zebrapro_announcements/config/StyleConfig.dart';
import 'package:zebrapro_announcements/routes/Routes.dart';
import 'package:zebrapro_announcements/ui/base/BaseView.dart';
import 'package:zebrapro_announcements/utils/ViewUtils.dart';

import 'ReportSearchViewModel.dart';

class ReportSearchView extends StatefulWidget {
  const ReportSearchView({Key? key}) : super(key: key);

  @override
  State<ReportSearchView> createState() => _HomeViewState();
}

class _HomeViewState extends State<ReportSearchView> {
  final _formKey = GlobalKey<FormBuilderState>();
  final _searchFieldKey = GlobalKey<FormBuilderFieldState>();
  final String addMore = "+ Add More";
  final List<String> _tags = [
    "+ Add More",
    "KPMG",
    "FICCI",
    "IBEF",
    "SEBI",
    "CRISIL",
    "CRISIL",
    "CITI",
    "PWC",
    "EDELWEISS",
    "MCKINSEY",
    "CARERATING",
    "ASSETS.EY",
    "DELOITTE",
  ];
  Offset? _tapPosition;
  final double _fontSize = 12;
  final List _icon = [Icons.home, Icons.language, Icons.headset];

  @override
  Widget build(BuildContext context) {
    return BaseView<ReportSearchViewModel>(
        onModelReady: (model) {},
        builder: (BuildContext context, model, Widget? child) {
          return FormBuilder(
            key: _formKey,
            child: Scaffold(
              body: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const SizedBox(
                      height: 52,
                    ),
                    SvgPicture.asset(
                      'assets/svg/research_report.svg',
                      height: ViewUtils.getHeightPercent(context, 20),
                    ),
                      const Padding(
                        padding: EdgeInsets.only(
                            left: 16.0, right: 16, top: 42, bottom: 4),
                        child: Text(
                          "Report Search",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16.0, right: 16, top: 4, bottom: 4),
                        child: Text(
                          "Find relevant reports for any given Industry",
                          textAlign: TextAlign.center,
                          style: StyleConfig.light(ColorConfig.boldTextColor),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16.0, right: 16.0, top: 24, bottom: 8),
                        child: FormBuilderTextField(
                          key: _searchFieldKey,
                          name: 'search',
                          onSubmitted: (str) {
                            if (str != null && str != "") {
                              Get.toNamed(RouteClass.getReportSearchRoute(),
                                  arguments: {
                                    "query": str,
                                    "tags": _tags.sublist(1)
                                  });
                            }
                          },
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.search,
                          decoration: StyleConfig.getInputDecoration(
                              'Search Report',
                              IconButton(
                                onPressed: () {
                                  if (_searchFieldKey.currentState != null &&
                                      _searchFieldKey.currentState!.value !=
                                          null) {
                                    Get.toNamed(
                                        RouteClass.getReportSearchRoute(),
                                        arguments: {
                                          "query": _searchFieldKey
                                              .currentState!.value,
                                          "tags": _tags.sublist(1)
                                        });
                                  }
                                },
                                icon: Icon(
                                  Icons.search,
                                  color: ColorConfig.darkGrey,
                                ),
                              )),
                          validator: FormBuilderValidators.compose(
                              [FormBuilderValidators.required()]),
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(
                            left: 16.0, right: 16, top: 16, bottom: 4),
                        child: Text(
                          "Publishers",
                          style: TextStyle(
                              fontWeight: FontWeight.w400, fontSize: 16),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 16.0, right: 16, bottom: 4),
                        child: Text(
                          "Find relevant reports for any given Industry",
                          style: StyleConfig.light(ColorConfig.boldTextColor),
                        ),
                      ),
                      const SizedBox(
                        height: 22,
                      ),
                      Tags(
                        key: const Key("1"),
                        columns: 6,
                        textDirection: TextDirection.ltr,
                        itemCount: _tags.length,
                        itemBuilder: (index) {
                          final item = _tags[index];

                          // if add more tag
                        if (_tags[index] == addMore) {
                          return buildAddMoreTagButton(index, item);
                        }

                        return buildTagPill(index, item);
                      },
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                  ],
                ),
              ),
              bottomNavigationBar: Padding(
                padding: const EdgeInsets.all(16.0),
                child: InkWell(
                  onTap: () {
                    if (_searchFieldKey.currentState != null &&
                        _searchFieldKey.currentState!.value != null) {
                      Get.toNamed(RouteClass.getReportSearchRoute(),
                          arguments: {
                            "query": _searchFieldKey.currentState!.value,
                            "tags": _tags.sublist(1)
                          });
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 56,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: ColorConfig.accentColor,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(12))),
                    child: const Text(
                      "Find Reports",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        });
  }

  GestureDetector buildTagPill(int index, String item) {
    return GestureDetector(
      onTapDown: (details) => _tapPosition = details.globalPosition,
      child: ItemTags(
        index: index,
        pressEnabled: true,
        title: item,
        elevation: 0,
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        key: Key(index.toString()),
        color: Colors.white,
        activeColor: ColorConfig.lightAccentColor,
        active: true,
        textActiveColor: ColorConfig.accentColor,
        textColor: ColorConfig.accentColor,
        border: Border.all(color: ColorConfig.lightAccentColor),
        // removeButton: ItemTagsRemoveButton(
        //   backgroundColor: ColorConfig.accentColor,
        //   onRemoved: () {
        //     setState(() {
        //       _tags.removeAt(index);
        //     });
        //     return true;
        //   },
        // ),
        textScaleFactor: utf8.encode(item.substring(0, 1)).length > 2 ? 0.8 : 1,
        textStyle: TextStyle(fontSize: _fontSize, fontWeight: FontWeight.w500),
      ),
    );
  }

  GestureDetector buildAddMoreTagButton(int index, String item) {
    return GestureDetector(
      onTapDown: (details) => _tapPosition = details.globalPosition,
      child: ItemTags(
        onPressed: (item) {
          showAddTagDialog();
        },
        index: index,
        title: addMore,
        pressEnabled: true,
        elevation: 0,
        padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 4),
        key: Key(index.toString()),
        color: ColorConfig.accentColor,
        activeColor: ColorConfig.accentColor,
        active: true,
        textActiveColor: ColorConfig.white,
        textColor: ColorConfig.white,
        border: Border.all(color: ColorConfig.accentColor),
        textScaleFactor: utf8.encode(item.substring(0, 1)).length > 2 ? 0.8 : 1,
        textStyle: TextStyle(fontSize: _fontSize, fontWeight: FontWeight.w500),
      ),
    );
  }

  void showAddTagDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(22),
          ),
          content: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(22),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  'Add New Tag',
                  style: StyleConfig.h1(ColorConfig.primaryTextColor),
                ),
                const SizedBox(
                  height: 12,
                ),
                FormBuilderTextField(
                  name: 'tag',
                  autofocus: true,
                  onSubmitted: (str) {
                    if (str != null && str != "") {
                      setState(() {
                        _tags.add(str.toUpperCase());
                      });
                    }
                    Navigator.pop(context);
                  },
                  decoration:
                      StyleConfig.getInputDecoration("Enter Tag Name", null),
                ),
                //
              ],
            ),
          ),
        );
      },
    );
  }

  TagsTextField get _textField {
    return TagsTextField(
      autofocus: false,
      width: 152,
      textStyle: TextStyle(fontSize: _fontSize, height: 0.0
          //height: 1
          ),
      inputDecoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(22),
          borderSide: BorderSide(color: ColorConfig.accentColor),
        ),
      ),
      enabled: true,
      constraintSuggestion: true,
      onSubmitted: (String str) {
        setState(() {
          _tags.add(str);
        });
      },
    );
  }
}
