import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:zebrapro_announcements/data/network/NetworkExceptions/NetworkExceptions.dart';
import 'package:zebrapro_announcements/data/network/model/ApiResult/ApiResult.dart';
import 'package:zebrapro_announcements/data/network/model/LoginRequestModel.dart';
import 'package:zebrapro_announcements/data/network/model/LoginResponseModel.dart';
import 'package:zebrapro_announcements/ui/base/BaseViewModel.dart';
import 'package:zebrapro_announcements/utils/enums/ViewState.dart';

class LoginViewModel extends BaseViewModel {
  void doLogin(String email, String password) async {
    setState(ViewState.busy);
    try {
      final credential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      if (credential.user != null) {
        String? uid = credential.user?.uid.toString();
        if (uid != null) {
          ApiResult<LoginResponseModel> result = await getDataManager()
              .loginApiCall(LoginRequestModel(username: email, password: uid));
          result.when(success: (LoginResponseModel model) async {
            setState(ViewState.idle);
            if (model.accessToken != null) {
              getDataManager().setServerAuthToken(model.accessToken!);
            }
            Get.offNamedUntil("/home", (route) => false);
          }, failure: (NetworkExceptions error) {
            setState(ViewState.idle);
            handleGeneralApiError(error, () {});
          });
        } else {
          setState(ViewState.idle);
          showErrorDialog(description: 'Unexpected Error Occurred. 35');
        }
      } else {
        setState(ViewState.idle);
        showErrorDialog(description: 'Unexpected Error Occurred. 39');
      }
    } on FirebaseAuthException catch (e) {
      setState(ViewState.idle);
      if (e.code == 'user-not-found') {
        showErrorDialog(description: 'No user found for that email.');
      } else if (e.code == 'wrong-password') {
        showErrorDialog(description: 'Wrong password provided for that user.');
      }
    }
  }
}
