import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shrink_sidemenu/shrink_sidemenu.dart';
import 'package:zebrapro_announcements/config/ColorConfig.dart';
import 'package:zebrapro_announcements/ui/base/BaseView.dart';
import 'package:zebrapro_announcements/ui/screens/announcements/AnnouncementsView.dart';
import 'package:zebrapro_announcements/ui/screens/home/HomeViewModel.dart';
import 'package:zebrapro_announcements/ui/screens/profile/ProfileView.dart';
import 'package:zebrapro_announcements/ui/screens/report_search/ReportSearchView.dart';
import 'package:zebrapro_announcements/ui/screens/states_scanner/StatesScannerView.dart';
import 'package:zebrapro_announcements/ui/widgets/nav_drawer/NavDrawer.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  int _currentIndex = 0;
  bool isOpened = false;
  final _screens = [
    AnnouncementsView(),
    const ReportSearchView(),
    const StatesScannerView(),
    const ProfileView()
  ];

  final GlobalKey<SideMenuState> _sideMenuKey = GlobalKey<SideMenuState>();
  final GlobalKey<SideMenuState> _endSideMenuKey = GlobalKey<SideMenuState>();

  toggleMenu([bool end = false]) {
    if (end) {
      final _state = _endSideMenuKey.currentState!;
      if (_state.isOpened) {
        _state.closeSideMenu();
      } else {
        _state.openSideMenu();
      }
    } else {
      final _state = _sideMenuKey.currentState!;
      if (_state.isOpened) {
        _state.closeSideMenu();
      } else {
        _state.openSideMenu();
      }
    }
  }

  void changeMenu(int screenIndex) {
    setState(() {
      _currentIndex = screenIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<HomeViewModel>(
        onModelReady: (model) {},
        builder: (BuildContext context, model, Widget? child) {
          return SideMenu(
            key: _sideMenuKey,
            menu: NavDrawer(
              changeMenu: changeMenu,
              toggleMenu: toggleMenu,
            ),
            type: SideMenuType.shrinkNSlide,
            background: Colors.grey.shade200,
            closeIconPositionInverse: true,
            closeIcon: Icon(
              Icons.close,
              color: ColorConfig.darkGrey,
            ),
            onChange: (_isOpened) {
              setState(() => isOpened = _isOpened);
            },
            child: IgnorePointer(
              ignoring: isOpened,
              child: Scaffold(
                resizeToAvoidBottomInset: false,
                appBar: AppBar(
                  elevation: 6,
                  leading: Padding(
                    padding: const EdgeInsets.only(left: 16.0),
                    child: SvgPicture.asset(
                      'assets/svg/notifications.svg',
                      semanticsLabel: "Notification",
                    ),
                  ),
                  leadingWidth: 44,
                  shadowColor: const Color(0x1A000000),
                  systemOverlayStyle: const SystemUiOverlayStyle(
                    statusBarColor: Colors.white,
                    statusBarIconBrightness: Brightness.dark,
                    // For Android (dark icons)
                    statusBarBrightness: Brightness.light,
                  ),
                  iconTheme: const IconThemeData(color: Colors.black87),
                  titleTextStyle: const TextStyle(color: Colors.black87),
                  centerTitle: true,
                  title: SvgPicture.asset(
                    "assets/svg/logo_with_name_h.svg",
                    semanticsLabel: 'ZebraPro',
                    height: 16,
                  ),
                  actions: [
                    IconButton(
                      icon: const Icon(Icons.menu),
                      onPressed: () => toggleMenu(false),
                    )
                  ],
                ),
                body: Stack(
                  children: [_screens[_currentIndex]],
                ),
              ),
            ),
          );
        });
  }
}
