import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zebrapro_announcements/config/ColorConfig.dart';
import 'package:zebrapro_announcements/ui/base/BaseView.dart';
import 'package:zebrapro_announcements/ui/screens/settings/SettingsViewModel.dart';

class SettingsView extends StatefulWidget {
  const SettingsView({Key? key}) : super(key: key);

  @override
  State<SettingsView> createState() => _SettingsViewState();
}

class _SettingsViewState extends State<SettingsView> {
  SettingsViewModel model = Get.find<SettingsViewModel>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<SettingsViewModel>(
      onModelReady: (model) async {},
      builder: (BuildContext context, model, Widget? child) {
        return Scaffold(
          appBar: AppBar(
            elevation: 6,
            shadowColor: const Color(0x1A000000),
            title: const Text("Settings"),
            iconTheme: const IconThemeData(color: Colors.black),
            titleTextStyle: TextStyle(color: Colors.black),
          ),
          body: Column(
            children: [
              ListTile(
                leading: const Icon(Icons.privacy_tip_outlined),
                title: const Text("Privacy & Policies"),
                onTap: () async {
                  if (await launchUrl(
                      Uri.parse("https://zebrapro.in/Privacy_Policy"))) {
                    Get.snackbar("Something went wrong", "Url can't open",
                        backgroundColor: ColorConfig.red);
                  }
                },
              ),
              ListTile(
                leading: const Icon(Icons.note_alt_outlined),
                title: const Text("Terms & Condition"),
                onTap: () async {
                  if (await launchUrl(Uri.parse("https://zebrapro.in/terms"))) {
                    Get.snackbar("Something went wrong", "Url can't open",
                        backgroundColor: ColorConfig.red);
                  }
                },
              ),
              const Spacer(),
              Center(
                child: SvgPicture.asset('assets/svg/logo_with_name_h.svg'),
              ),
              const SizedBox(
                height: 12,
              ),
              Text("© Copyright " +
                  DateTime.now().year.toString() +
                  " ZebraLearn Pvt. Ltd"),
              const SizedBox(
                height: 12,
              )
            ],
          ),
        );
      },
    );
  }
}
