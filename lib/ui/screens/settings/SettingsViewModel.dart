import 'package:zebrapro_announcements/ui/base/BaseViewModel.dart';

class SettingsViewModel extends BaseViewModel {
  setToken(String token) {
    getDataManager().setServerAuthToken(token);
  }

  String? getToken() {
    return getDataManager().getServerAuthToken();
  }
}
