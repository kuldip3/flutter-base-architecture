import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zebrapro_announcements/ui/base/BaseView.dart';

import 'StartupViewModel.dart';

class StartupView extends StatefulWidget {
  const StartupView({Key? key}) : super(key: key);

  @override
  State<StartupView> createState() => _StartupViewState();
}

class _StartupViewState extends State<StartupView> {
  StartupViewModel model = Get.find<StartupViewModel>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<StartupViewModel>(
      onModelReady: (model) async {
        Future.delayed(Duration.zero, () {
          model.runStartupLogic();
        });
      },
      builder: (BuildContext context, model, Widget? child) {
        return Scaffold(
          body: Center(
            child: Image.asset('assets/images/logo.png'),
          ),
        );
      },
    );
  }
}
