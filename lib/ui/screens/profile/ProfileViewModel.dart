import 'package:zebrapro_announcements/data/network/NetworkExceptions/NetworkExceptions.dart';
import 'package:zebrapro_announcements/data/network/model/ApiResult/ApiResult.dart';
import 'package:zebrapro_announcements/data/network/model/ProfileDataResponseModel.dart';
import 'package:zebrapro_announcements/ui/base/BaseViewModel.dart';
import 'package:zebrapro_announcements/utils/enums/ViewState.dart';

class ProfileViewModel extends BaseViewModel {
  Future<ProfileDataResponseModel?>? getProfileData() async {
    ProfileDataResponseModel? responseModel;
    setState(ViewState.busy);

    ApiResult<ProfileDataResponseModel> result =
        await getDataManager().getProfileData();
    responseModel =
        await result.when(success: (ProfileDataResponseModel model) async {
      setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      setState(ViewState.idle);
      handleGeneralApiError(error, () {});
      return null;
    });

    return responseModel;
  }
}
