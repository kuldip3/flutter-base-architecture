import 'package:flutter/material.dart';
import 'package:zebrapro_announcements/config/ColorConfig.dart';
import 'package:zebrapro_announcements/data/network/model/ProfileDataResponseModel.dart';
import 'package:zebrapro_announcements/ui/base/BaseView.dart';
import 'package:zebrapro_announcements/ui/screens/profile/ProfileViewModel.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({Key? key}) : super(key: key);

  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  late ProfileViewModel _model;

  ProfileData? _profileResponse;

  @override
  Widget build(BuildContext context) {
    return BaseView<ProfileViewModel>(onModelReady: (model) async {
      _model = model;
      ProfileDataResponseModel? response = await _model.getProfileData();
      if (response != null &&
          response.data != null &&
          response.data!.profileData != null) {
        setState(() {
          _profileResponse = response.data!.profileData;
        });
      }
    }, builder: (BuildContext context, model, Widget? child) {
      return _profileResponse != null
          ? Scaffold(
              backgroundColor: const Color(0xfff9f9f9),
              body: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: SizedBox(
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 52),
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(22)),
                                  clipBehavior: Clip.hardEdge,
                                  child: const Image(
                                    height: 132,
                                    width: 132,
                                    image: NetworkImage(
                                        "https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png"),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 12,
                              ),
                              Text(
                                _profileResponse!.name!,
                                style: const TextStyle(
                                    fontSize: 22, fontWeight: FontWeight.w500),
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              Text(
                                _profileResponse!.companyInfo!.designation!,
                                style: const TextStyle(
                                    fontSize: 19, fontWeight: FontWeight.w300),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 22,
                        ),
                        Card(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12)),
                          child: SizedBox(
                            width: double.infinity,
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    "Email",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w300),
                                  ),
                                  const SizedBox(
                                    height: 6,
                                  ),
                                  Text(
                                    _profileResponse!.email!,
                                    style: const TextStyle(
                                        fontSize: 19,
                                        fontWeight: FontWeight.w400),
                                  ),
                                  const SizedBox(
                                    height: 22,
                                  ),
                                  const Text(
                                    "Phone",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w300),
                                  ),
                                  const SizedBox(
                                    height: 6,
                                  ),
                                  Text(
                                    _profileResponse!.phone!,
                                    style: const TextStyle(
                                        fontSize: 19,
                                        fontWeight: FontWeight.w400),
                                  ),
                                  const SizedBox(
                                    height: 22,
                                  ),
                                  const Text(
                                    "Company",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w300),
                                  ),
                                  const SizedBox(
                                    height: 6,
                                  ),
                                  Text(
                                    _profileResponse!.companyInfo!.companyName!,
                                    style: const TextStyle(
                                        fontSize: 19,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        Card(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12)),
                          child: SizedBox(
                            width: double.infinity,
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Text(
                                "Change Password",
                                style:
                                    TextStyle(color: ColorConfig.accentColor),
                              ),
                            ),
                          ),
                        ),
                        Card(
                          elevation: 0,
                          color: Color(0xFFFFF2F2),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: SizedBox(
                            width: double.infinity,
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Text(
                                "Log Out",
                                style:
                                    TextStyle(color: ColorConfig.accentColor),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            )
          : Container();
    });
  }
}
