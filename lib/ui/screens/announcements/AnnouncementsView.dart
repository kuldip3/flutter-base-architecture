import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:flutter_svg/svg.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zebrapro_announcements/config/ApiConfig.dart';
import 'package:zebrapro_announcements/config/ColorConfig.dart';
import 'package:zebrapro_announcements/config/StyleConfig.dart';
import 'package:zebrapro_announcements/data/network/model/AnnouncementsResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/BookmarkSaveResponse.dart';
import 'package:zebrapro_announcements/ui/base/BaseView.dart';
import 'package:zebrapro_announcements/ui/screens/announcements/AnnouncemetsViewModel.dart';
import 'package:zebrapro_announcements/ui/widgets/AppPdfViewer.dart';
import 'package:zebrapro_announcements/utils/ViewUtils.dart';
import 'package:zebrapro_announcements/utils/enums/ViewState.dart';

class AnnouncementsView extends StatefulWidget {
  AnnouncementsResponseModel? _responseModel;

  AnnouncementsView({Key? key}) : super(key: key);

  @override
  State<AnnouncementsView> createState() => _HomeViewState();
}

class _HomeViewState extends State<AnnouncementsView> {
  String? selectedTab = "All";
  final GlobalKey<ScaffoldState> _key = GlobalKey();

  late AnnouncementViewModel _model; // Create a key
  late List<Map<String, dynamic>> tagList =
      AnnouncementViewModel.getFiltersTagsMap();

  int bookmarkButtonState = 0;

  late int roleId = 6;

  final GlobalKey<FormBuilderFieldState> _impactFieldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return BaseView<AnnouncementViewModel>(onModelReady: (model) async {
      _model = model;
      if (widget._responseModel == null) {
        await getAnnouncements(
            onlyImpacts: false, sentiment: selectedTab, tagList: tagList);
      }
      if (_model.getDataManager().getRoleId() != null) {
        roleId = _model.getDataManager().getRoleId()!;
      }
    }, builder: (BuildContext context, model, Widget? child) {
      if (model.state == ViewState.busy) {
        EasyLoading.show(status: 'Please wait...');
      } else {
        EasyLoading.dismiss();
        if (widget._responseModel == null) {
          return const Center(
            child: Text("Sorry No Content Found"),
          );
        }
      }

      return widget._responseModel != null &&
              widget._responseModel?.data != null
          ? Scaffold(
              key: _key,
              onEndDrawerChanged: (isOpen) async {
                if (!isOpen) {
                  await getAnnouncements(
                      onlyImpacts: selectedTab == "High Impact",
                      sentiment:
                          selectedTab == "High Impact" ? "All" : selectedTab,
                      tagList: tagList);
                }
              },
              endDrawer: Drawer(
                child: ListView(
                  physics: const BouncingScrollPhysics(),
                  padding: EdgeInsets.zero,
                  children: [
                    ...tagList.map((e) {
                      return CheckboxListTile(
                        title: Text(e['name']),
                        checkColor: ColorConfig.white,
                        selectedTileColor: ColorConfig.accentColor,
                        activeColor: ColorConfig.accentColor,
                        value: e['selected'],
                        onChanged: (val) {
                          setState(() {
                            e['selected'] = val;
                          });
                        },
                      );
                    }).toList(),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Container(
                        height: 52,
                        child: ElevatedButton(
                          onPressed: () async {
                            await getAnnouncements(
                                onlyImpacts: selectedTab == "High Impact",
                                sentiment: selectedTab == "High Impact"
                                    ? "All"
                                    : selectedTab,
                                tagList: tagList);
                            _key.currentState!.closeEndDrawer();
                          },
                          child: Text("Apply"),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              body: NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverAppBar(
                      expandedHeight: 72.0,
                      floating: false,
                      pinned: false,
                      flexibleSpace: getTabBar(),
                    ),
                  ];
                },
                body: RefreshIndicator(
                  color: ColorConfig.accentColor,
                  onRefresh: () async {
                    await getAnnouncements(
                        onlyImpacts: selectedTab == "High Impact",
                        sentiment:
                            selectedTab == "High Impact" ? "All" : selectedTab,
                        tagList: tagList);
                  },
                  child: Container(
                    color: Colors.white,
                    child: ListView.separated(
                      physics: const BouncingScrollPhysics(),
                      itemCount: widget._responseModel!.data!.result!.length,
                      itemBuilder: (context, index) {
                        Result? result =
                            widget._responseModel?.data?.result![index];
                        return result != null
                            ? buildAnnouncementCard(result)
                            : const Text("Error Occurred");
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          const SizedBox(
                        height: 8,
                      ),
                    ),
                  ),
                ),
              ),
            )
          : Container();
    });
  }

  FlexibleSpaceBar getTabBar() {
    return FlexibleSpaceBar(
      centerTitle: true,
      background: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Row(
            children: [
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () async {
                      setState(() {
                        selectedTab = "All";
                      });
                      await getAnnouncements(
                          onlyImpacts: false,
                          sentiment: selectedTab,
                          tagList: tagList);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: selectedTab == "All"
                                  ? ColorConfig.secondaryTextColor
                                  : ColorConfig.lightGrey),
                          borderRadius: BorderRadius.circular(4)),
                      padding: EdgeInsets.all(4),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Text(
                          "All",
                          style: StyleConfig.h3(selectedTab == "All"
                              ? ColorConfig.boldTextColor
                              : ColorConfig.secondaryTextColor),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  InkWell(
                    onTap: () async {
                      setState(() {
                        selectedTab = "High Impact";
                      });
                      await getAnnouncements(
                          onlyImpacts: true,
                          sentiment: "All",
                          tagList: tagList);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: selectedTab == "High Impact"
                                  ? ColorConfig.secondaryTextColor
                                  : ColorConfig.lightGrey),
                          borderRadius: BorderRadius.circular(4)),
                      padding: EdgeInsets.all(4),
                      child: Text(
                        "High Impact",
                        style: StyleConfig.h3(selectedTab == "High Impact"
                            ? ColorConfig.boldTextColor
                            : ColorConfig.secondaryTextColor),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  InkWell(
                    onTap: () async {
                      setState(() {
                        selectedTab = "Positive";
                      });
                      await getAnnouncements(
                          onlyImpacts: false,
                          sentiment: selectedTab,
                          tagList: tagList);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: selectedTab == "Positive"
                                  ? ColorConfig.secondaryTextColor
                                  : ColorConfig.lightGrey),
                          borderRadius: BorderRadius.circular(4)),
                      padding: EdgeInsets.all(4),
                      child: Text(
                        "Positive",
                        style: StyleConfig.h3(selectedTab == "Positive"
                            ? ColorConfig.boldTextColor
                            : ColorConfig.secondaryTextColor),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  InkWell(
                    onTap: () async {
                      setState(() {
                        selectedTab = "Negative";
                      });
                      await getAnnouncements(
                          onlyImpacts: false,
                          sentiment: selectedTab,
                          tagList: tagList);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: selectedTab == "Negative"
                                  ? ColorConfig.secondaryTextColor
                                  : ColorConfig.lightGrey),
                          borderRadius: BorderRadius.circular(4)),
                      padding: EdgeInsets.all(4),
                      child: Text(
                        "Negative",
                        style: StyleConfig.h3(selectedTab == "Negative"
                            ? ColorConfig.boldTextColor
                            : ColorConfig.secondaryTextColor),
                      ),
                    ),
                  ),
                ],
              ),
              const Spacer(),
              InkWell(
                onTap: () {
                  if (_key.currentState!.isEndDrawerOpen) {
                    _key.currentState!.closeEndDrawer();
                  } else {
                    _key.currentState!.openEndDrawer();
                  }
                },
                child: SvgPicture.asset('assets/svg/funnel.svg'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> getAnnouncements(
      {required bool onlyImpacts,
      required String? sentiment,
      required List<Map<String, dynamic>> tagList}) async {
    AnnouncementsResponseModel? response = await _model.fetchAnnouncements(
        tagList
            .where((element) => element['selected'])
            .map<String>((e) => e['name'])
            .toList(),
        onlyImpacts,
        sentiment);
    if (response != null) {
      setState(() {
        widget._responseModel = response;
      });
    }
  }

  Widget buildAnnouncementCard(Result result) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 6),
      child: Card(
        elevation: 6,
        shadowColor: const Color(0x40000000),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(22))),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(22.0),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              getAnnouncementHeader(result),
              const SizedBox(
                height: 16,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24.0),
                child: Row(
                  children: [
                    ...result.tags!.split(",").map<Widget>((tag) {
                      return Padding(
                        padding: const EdgeInsets.only(right: 4.0),
                        child: Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.all(4),
                          decoration: BoxDecoration(
                              color: ColorConfig.lightAccentColor,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(12))),
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 6.0),
                            child: Text(
                              tag,
                              style: TextStyle(
                                  color: ColorConfig.accentColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      );
                    }).toList(),
                  ],
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 24.0, right: 24.0, top: 12, bottom: 4),
                child: Text(
                  result.newssub!,
                  textAlign: TextAlign.justify,
                  softWrap: true,
                  style: TextStyle(
                      color: ColorConfig.primaryTextColor,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 24.0, right: 24.0, top: 12, bottom: 8),
                child: Text(
                  result.newsdt!,
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.normal,
                      color: ColorConfig.secondaryTextColor),
                ),
              ),
              if (result.impact != null) getAnnouncementImpacts(result),
              const SizedBox(
                height: 16,
              ),
              getAnnouncementActions(result)
            ],
          ),
        ),
      ),
    );
  }

  /// Announcement Buttons
  Padding getAnnouncementActions(Result result) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: InkWell(
              onTap: () {
                _launchUrl(Uri.parse(
                    "https://www.screener.in/company/${result.companyId}/"));
              },
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: ColorConfig.lightGrey),
                    borderRadius: BorderRadius.circular(4)),
                padding: EdgeInsets.all(4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset('assets/svg/screener.svg',
                        width: 20, color: Colors.black, semanticsLabel: 'pdf'),
                    const SizedBox(
                      width: 2,
                    ),
                    const Text(
                      "Screener",
                      style: TextStyle(fontSize: 10),
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            width: 2,
          ),
          Expanded(
            child: InkWell(
              onTap: () {
                Get.to(() => AppPdfViewer(
                    pdfUrl: result.pdfLink!, title: result.slongname!));
              },
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: ColorConfig.lightGrey),
                    borderRadius: BorderRadius.circular(4)),
                padding: EdgeInsets.all(4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset('assets/svg/filetype-pdf 1.svg',
                        width: 20, semanticsLabel: 'pdf'),
                    const SizedBox(
                      width: 2,
                    ),
                    const Text(
                      "Report",
                      style: TextStyle(fontSize: 10),
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            width: 2,
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: ColorConfig.lightGrey),
                  borderRadius: BorderRadius.circular(4)),
              padding: EdgeInsets.all(4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset('assets/svg/chat-left-text 1.svg',
                      width: 20, semanticsLabel: 'bookmark'),
                  const SizedBox(
                    width: 2,
                  ),
                  const Text(
                    "Comment",
                    style: TextStyle(fontSize: 10),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            width: 2,
          ),
          Expanded(
            child: InkWell(
              onTap: () async {
                BookmarkSaveResponse? bookmarkResponse =
                    await _model.postBookmarkSearchResult(
                        resultId: result.id!, tableName: 'announcements');
                if (bookmarkResponse != null && bookmarkResponse.data != null) {
                  setState(() {
                    bookmarkButtonState =
                        bookmarkResponse.data!.favourite! ? 1 : 0;
                  });

                  String title = bookmarkResponse.data!.favourite!
                      ? "Bookmarked"
                      : "Removed from Bookmarks";
                  Get.snackbar("Success", "This post is " + title,
                      backgroundColor: bookmarkResponse.data!.favourite!
                          ? Colors.greenAccent
                          : Colors.redAccent);
                }
              },
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(
                      color: bookmarkButtonState == 0
                          ? ColorConfig.lightGrey
                          : ColorConfig.accentColor,
                    ),
                    borderRadius: BorderRadius.circular(4)),
                padding: EdgeInsets.all(4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset('assets/svg/bookmark-plus 1.svg',
                        color: bookmarkButtonState == 0
                            ? ColorConfig.primaryTextColor
                            : ColorConfig.accentColor,
                        width: 20,
                        semanticsLabel: 'bookmark'),
                    const SizedBox(
                      width: 2,
                    ),
                    Text(
                      "Save",
                      style: TextStyle(
                        fontSize: 10,
                        color: bookmarkButtonState == 0
                            ? ColorConfig.primaryTextColor
                            : ColorConfig.accentColor,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _launchUrl(Uri _url) async {
    if (!await launchUrl(_url)) {
      throw 'Could not launch $_url';
    }
  }

  /// Impacts
  Padding getAnnouncementImpacts(Result result) {
    return Padding(
      padding:
          const EdgeInsets.only(left: 24.0, right: 24.0, top: 12, bottom: 4),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xFFF5F5F5),
          borderRadius: BorderRadius.circular(12),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Impacts:",
                style: TextStyle(
                    color: ColorConfig.accentColor,
                    fontSize: 16,
                    fontWeight: FontWeight.w700),
              ),
              SizedBox(
                height: 8,
              ),
              ...result.impact!.split("\n").map((e) => Padding(
                    padding: const EdgeInsets.symmetric(vertical: 4.0),
                    child: Row(
                      children: [
                        Container(
                          width: 5,
                          height: 5,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: ColorConfig.accentColor,
                          ),
                        ),
                        SizedBox(
                          width: 6,
                        ),
                        Text(
                          "${e}",
                          style: TextStyle(color: ColorConfig.primaryTextColor),
                        )
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }

  /// Header
  Stack getAnnouncementHeader(Result result) {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 12),
          child: Align(
            alignment: Alignment.topLeft,
            child: result.logoUrl != null
                ? Container(
                    clipBehavior: Clip.hardEdge,
                    width: 38,
                    height: 38,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade300,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: SvgPicture.network(
                      ApiConfig.S3_URL + result.logoUrl!,
                      height: 38,
                      width: 38,
                    ),
                  )
                : Container(
                    width: 38,
                    height: 38,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade300,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text((result.slongname!.substring(0, 1)),
                        style: const TextStyle(color: Colors.black)),
                  ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 74, top: 16, right: 68),
          child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  result.slongname!.length > 30
                      ? result.slongname!.substring(0, 30)
                      : result.slongname!,
                  overflow: TextOverflow.ellipsis,
                  style: StyleConfig.h3(ColorConfig.boldTextColor),
                ),
                const SizedBox(
                  height: 4,
                ),
                Text(
                  result.name!,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: ColorConfig.secondaryTextColor,
                      fontSize: 10,
                      fontWeight: FontWeight.w300),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 40, top: 16),
          child: Align(
            alignment: Alignment.topRight,
            child: Container(
              width: 28,
              height: 28,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
              ),
              child: result.sentimentScore! == 0
                  ? null
                  : result.sentimentScore! > 0
                      ? Tooltip(
                          triggerMode: TooltipTriggerMode.tap,
                          message: "Positive",
                          child: SvgPicture.asset(
                            'assets/svg/positive_announcement.svg',
                            width: 28,
                            height: 28,
                          ),
                        )
                      : Tooltip(
                          triggerMode: TooltipTriggerMode.tap,
                          message: "Negative",
                          child: SvgPicture.asset(
                            'assets/svg/negative_announcement.svg',
                            width: 28,
                            height: 28,
                          ),
                        ),
            ),
          ),
        ),
        if (roleId < 4)
          Padding(
            padding: const EdgeInsets.only(right: 6, top: 6),
            child: Align(
              alignment: Alignment.topRight,
              child: PopupMenuButton(
                  padding: const EdgeInsets.all(0),
                  icon: Icon(Icons.more_vert),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  itemBuilder: (_) => <PopupMenuItem<String>>[
                        const PopupMenuItem<String>(
                            child: Text('Add Impact'), value: 'add'),
                        if (result.impactId != null)
                          PopupMenuItem<String>(
                              child: Text(
                                'Delete Impact',
                                style: TextStyle(color: ColorConfig.red),
                              ),
                              value: 'delete'),
                      ],
                  onSelected: (menu) {
                    switch (menu) {
                      case "add":
                        showImpactDialog(result);
                        break;
                      case "delete":
                        if (result.impactId != null)
                          showConfirmDeleteDialog(result.impactId!);
                        break;
                      default:
                        break;
                    }
                  }),
            ),
          )
      ],
    );
  }

  void showImpactDialog(Result result) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(22),
          ),
          child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              Container(
                width: ViewUtils.getWidthPercent(context, 90),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(22),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        'Add Impact of this announcement',
                        style: StyleConfig.h3(ColorConfig.primaryTextColor),
                      ),
                      const SizedBox(
                        height: 32,
                      ),
                      FormBuilderTextField(
                        name: 'tag',
                        key: _impactFieldKey,
                        autofocus: true,
                        initialValue: result.impact ?? null,
                        keyboardType: TextInputType.multiline,
                        maxLines: 5,
                        textInputAction: TextInputAction.newline,
                        validator: FormBuilderValidators.compose(
                            [FormBuilderValidators.required()]),
                        decoration: StyleConfig.getInputDecoration(
                            "Enter Impact", null),
                      ),
                      const SizedBox(
                        height: 16,
                      ),
                      InkWell(
                        onTap: () async {
                          if (_impactFieldKey.currentState!.validate()) {
                            await _model.postImpactOnAnnouncement(
                                resultId: result.id!,
                                body: _impactFieldKey.currentState!.value);
                            await getAnnouncements(
                                onlyImpacts: selectedTab == "High Impact",
                                sentiment: selectedTab == "High Impact"
                                    ? "All"
                                    : selectedTab,
                                tagList: tagList);
                            Navigator.pop(context);
                          }
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 56,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: ColorConfig.accentColor,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(12))),
                          child: const Text(
                            "Save Impact",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  void showConfirmDeleteDialog(int id) {
    showDialog<void>(
      context: context,
      barrierDismissible: true,
      // false = user must tap button, true = tap outside dialog
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(22)),
          title: const Text('Are you sure?'),
          content: const Text('Do you really want to delete this impact'),
          actions: <Widget>[
            TextButton(
              child: Text(
                'YES',
                style: TextStyle(color: ColorConfig.red),
              ),
              onPressed: () async {
                await _model.deleteImpactOnAnnouncement(resultId: id);
                await getAnnouncements(
                    onlyImpacts: selectedTab == "High Impact",
                    sentiment:
                        selectedTab == "High Impact" ? "All" : selectedTab,
                    tagList: tagList);
                Navigator.of(dialogContext).pop(); // Dismiss alert dialog
              },
            ),
            TextButton(
              child: Text(
                'NO',
                style: TextStyle(color: ColorConfig.primaryTextColor),
              ),
              onPressed: () {
                Navigator.of(dialogContext).pop(); // Dismiss alert dialog
              },
            ),
          ],
        );
      },
    );
  }
}
