import 'package:zebrapro_announcements/data/network/NetworkExceptions/NetworkExceptions.dart';
import 'package:zebrapro_announcements/data/network/model/ApiResult/ApiResult.dart';
import 'package:zebrapro_announcements/data/network/model/BookmarkSaveResponse.dart';
import 'package:zebrapro_announcements/data/network/model/requests/BookmarkRequest.dart';
import 'package:zebrapro_announcements/ui/base/BaseViewModel.dart';

class AnnouncementCardViewModel extends BaseViewModel {
  Future<BookmarkSaveResponse?>? postBookmarkSearchResult(
      {required int resultId, required String tableName}) async {
    BookmarkSaveResponse? responseModel;
    // setState(ViewState.busy);
    BookmarkRequest request =
        BookmarkRequest(postId: resultId, tableName: tableName);

    ApiResult<BookmarkSaveResponse> result =
        await getDataManager().postBookmarkItemApiCall(request);
    responseModel =
        await result.when(success: (BookmarkSaveResponse model) async {
      // setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      // setState(ViewState.idle);
      handleGeneralApiError(error, () {});
      return null;
    });

    return responseModel;
  }
}
