import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zebrapro_announcements/config/ApiConfig.dart';
import 'package:zebrapro_announcements/config/ColorConfig.dart';
import 'package:zebrapro_announcements/config/StyleConfig.dart';
import 'package:zebrapro_announcements/data/network/model/AnnouncementsResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/BookmarkSaveResponse.dart';
import 'package:zebrapro_announcements/ui/base/BaseView.dart';
import 'package:zebrapro_announcements/ui/screens/announcements/announcement_card/AnnouncementCardViewModel.dart';
import 'package:zebrapro_announcements/ui/widgets/AppPdfViewer.dart';

class AnnouncementCardView extends StatefulWidget {
  Result result;

  AnnouncementCardView({Key? key, required this.result}) : super(key: key);

  @override
  State<AnnouncementCardView> createState() => _AnnouncementCardViewState();
}

class _AnnouncementCardViewState extends State<AnnouncementCardView> {
  late AnnouncementCardViewModel _model;
  int bookmarkButtonState = 0;

  late int roleId = 6;

  @override
  Widget build(BuildContext context) {
    return BaseView<AnnouncementCardViewModel>(onModelReady: (model) async {
      _model = model;
      if (widget.result.save != null) {
        setState(() {
          bookmarkButtonState = widget.result.save!;
        });
      }
      if (_model.getDataManager().getRoleId() != null) {
        roleId = _model.getDataManager().getRoleId()!;
      }
    }, builder: (BuildContext context, model, Widget? child) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 6),
        child: Card(
          elevation: 6,
          shadowColor: const Color(0x40000000),
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(22))),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(22.0),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                getAnnouncementHeader(widget.result),
                const SizedBox(
                  height: 16,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24.0),
                  child: Row(
                    children: [
                      ...widget.result.tags!.split(",").map<Widget>((tag) {
                        return Padding(
                          padding: const EdgeInsets.only(right: 4.0),
                          child: Container(
                            alignment: Alignment.center,
                            padding: const EdgeInsets.all(4),
                            decoration: BoxDecoration(
                                color: ColorConfig.lightAccentColor,
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(12))),
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 6.0),
                              child: Text(
                                tag,
                                style: TextStyle(
                                    color: ColorConfig.accentColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                        );
                      }).toList(),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 24.0, right: 24.0, top: 12, bottom: 4),
                  child: Text(
                    widget.result.newssub!,
                    textAlign: TextAlign.justify,
                    softWrap: true,
                    style: TextStyle(
                        color: ColorConfig.primaryTextColor,
                        fontSize: 14,
                        fontWeight: FontWeight.w400),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 24.0, right: 24.0, top: 12, bottom: 8),
                  child: Text(
                    widget.result.newsdt!,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                        color: ColorConfig.secondaryTextColor),
                  ),
                ),
                if (widget.result.impact != null)
                  getAnnouncementImpacts(widget.result),
                const SizedBox(
                  height: 16,
                ),
                getAnnouncementActions(widget.result)
              ],
            ),
          ),
        ),
      );
    });
  }

  /// Announcement Buttons
  Padding getAnnouncementActions(Result result) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: InkWell(
              onTap: () {
                _launchUrl(Uri.parse(
                    "https://www.screener.in/company/${result.companyId}/"));
              },
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: ColorConfig.lightGrey),
                    borderRadius: BorderRadius.circular(4)),
                padding: const EdgeInsets.all(4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset('assets/svg/screener.svg',
                        width: 20, color: Colors.black, semanticsLabel: 'pdf'),
                    const SizedBox(
                      width: 2,
                    ),
                    const Text(
                      "Screener",
                      style: TextStyle(fontSize: 10),
                    )
                  ],
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 2,
          ),
          Expanded(
            child: InkWell(
              onTap: () {
                Get.to(() => AppPdfViewer(
                    pdfUrl: result.pdfLink!, title: result.slongname!));
              },
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: ColorConfig.lightGrey),
                    borderRadius: BorderRadius.circular(4)),
                padding: const EdgeInsets.all(4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset('assets/svg/filetype-pdf 1.svg',
                        width: 20, semanticsLabel: 'pdf'),
                    const SizedBox(
                      width: 2,
                    ),
                    const Text(
                      "Report",
                      style: TextStyle(fontSize: 10),
                    )
                  ],
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 2,
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: ColorConfig.lightGrey),
                  borderRadius: BorderRadius.circular(4)),
              padding: const EdgeInsets.all(4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset('assets/svg/chat-left-text 1.svg',
                      width: 20, semanticsLabel: 'bookmark'),
                  const SizedBox(
                    width: 2,
                  ),
                  const Text(
                    "Comment",
                    style: TextStyle(fontSize: 10),
                  )
                ],
              ),
            ),
          ),
          const SizedBox(
            width: 2,
          ),
          Expanded(
            child: InkWell(
              onTap: () async {
                BookmarkSaveResponse? bookmarkResponse =
                    await _model.postBookmarkSearchResult(
                        resultId: result.id!, tableName: 'announcements');
                if (bookmarkResponse != null && bookmarkResponse.data != null) {
                  setState(() {
                    bookmarkButtonState =
                        bookmarkResponse.data!.favourite! ? 1 : 0;
                  });
                  String title = bookmarkResponse.data!.favourite!
                      ? "Bookmarked"
                      : "Removed from Bookmarks";
                  Get.snackbar("Success", "This post is " + title,
                      backgroundColor: bookmarkResponse.data!.favourite!
                          ? Colors.greenAccent
                          : Colors.redAccent);
                }
              },
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(
                      color: bookmarkButtonState == 0
                          ? ColorConfig.lightGrey
                          : ColorConfig.accentColor,
                    ),
                    borderRadius: BorderRadius.circular(4)),
                padding: const EdgeInsets.all(4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset('assets/svg/bookmark-plus 1.svg',
                        color: bookmarkButtonState == 0
                            ? ColorConfig.primaryTextColor
                            : ColorConfig.accentColor,
                        width: 20,
                        semanticsLabel: 'bookmark'),
                    const SizedBox(
                      width: 2,
                    ),
                    Text(
                      bookmarkButtonState == 0 ? "Save" : "Saved",
                      style: TextStyle(
                        fontSize: 10,
                        color: bookmarkButtonState == 0
                            ? ColorConfig.primaryTextColor
                            : ColorConfig.accentColor,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _launchUrl(Uri _url) async {
    if (!await launchUrl(_url)) {
      throw 'Could not launch $_url';
    }
  }

  /// Impacts
  Padding getAnnouncementImpacts(Result result) {
    return Padding(
      padding:
          const EdgeInsets.only(left: 24.0, right: 24.0, top: 12, bottom: 4),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: const Color(0xFFF5F5F5),
          borderRadius: BorderRadius.circular(12),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Impacts:",
                style: TextStyle(
                    color: ColorConfig.accentColor,
                    fontSize: 16,
                    fontWeight: FontWeight.w700),
              ),
              const SizedBox(
                height: 8,
              ),
              ...result.impact!.split("\n").map((e) => Padding(
                    padding: const EdgeInsets.symmetric(vertical: 4.0),
                    child: Row(
                      children: [
                        Container(
                          width: 5,
                          height: 5,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: ColorConfig.accentColor,
                          ),
                        ),
                        const SizedBox(
                          width: 6,
                        ),
                        Text(
                          e,
                          style: TextStyle(color: ColorConfig.primaryTextColor),
                        )
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }

  /// Header
  Stack getAnnouncementHeader(Result result) {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 12),
          child: Align(
            alignment: Alignment.topLeft,
            child: result.logoUrl != null && result.logoUrl != ""
                ? Container(
                    clipBehavior: Clip.hardEdge,
                    width: 38,
                    height: 38,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade300,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: SvgPicture.network(
                      ApiConfig.S3_URL + result.logoUrl!,
                      height: 38,
                      width: 38,
                    ),
                  )
                : Container(
                    width: 38,
                    height: 38,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade300,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text(
                        "${result.slongname != null ? result.slongname!.substring(0, 1) : "Z"}",
                        style: const TextStyle(color: Colors.black)),
                  ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 74, top: 16, right: 68),
          child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  result.slongname != null
                      ? (result.slongname!.length > 30
                          ? result.slongname!.substring(0, 30)
                          : result.slongname!)
                      : "",
                  overflow: TextOverflow.ellipsis,
                  style: StyleConfig.h3(ColorConfig.boldTextColor),
                ),
                const SizedBox(
                  height: 4,
                ),
                Text(
                  result.name != null ? result.name! : "",
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: ColorConfig.secondaryTextColor,
                      fontSize: 10,
                      fontWeight: FontWeight.w300),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 40, top: 16),
          child: Align(
            alignment: Alignment.topRight,
            child: Container(
              width: 28,
              height: 28,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
              ),
              child:
                  result.sentimentScore == null || result.sentimentScore! == 0
                      ? null
                      : result.sentimentScore! > 0
                          ? Tooltip(
                              triggerMode: TooltipTriggerMode.tap,
                              message: "Positive",
                              child: SvgPicture.asset(
                                'assets/svg/positive_announcement.svg',
                                width: 28,
                                height: 28,
                              ),
                            )
                          : Tooltip(
                              triggerMode: TooltipTriggerMode.tap,
                              message: "Negative",
                              child: SvgPicture.asset(
                                'assets/svg/negative_announcement.svg',
                                width: 28,
                                height: 28,
                              ),
                            ),
            ),
          ),
        ),
        if (roleId < 4)
          Padding(
            padding: const EdgeInsets.only(right: 6, top: 6),
            child: Align(
              alignment: Alignment.topRight,
              child: PopupMenuButton(
                  padding: const EdgeInsets.all(0),
                  icon: const Icon(Icons.more_vert),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  itemBuilder: (_) => <PopupMenuItem<String>>[
                        const PopupMenuItem<String>(
                            child: Text('Add Impact'), value: 'add'),
                        PopupMenuItem<String>(
                            child: Text(
                              'Delete Impact',
                              style: TextStyle(color: ColorConfig.red),
                            ),
                            value: 'delete'),
                      ],
                  onSelected: (menu) {
                    switch (menu) {
                      case "add":
                        break;
                      case "delete":
                        break;
                      default:
                        break;
                    }
                  }),
            ),
          )
      ],
    );
  }
}
