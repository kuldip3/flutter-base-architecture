import 'package:intl/intl.dart';
import 'package:zebrapro_announcements/data/network/NetworkExceptions/NetworkExceptions.dart';
import 'package:zebrapro_announcements/data/network/model/AnnouncementCommentsResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/AnnouncementSaveCommentResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/AnnouncementsResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ApiResult/ApiResult.dart';
import 'package:zebrapro_announcements/data/network/model/BookmarkSaveResponse.dart';
import 'package:zebrapro_announcements/data/network/model/SaveImpactResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/requests/AnnouncementCommentRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/AnnouncementImpactRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/AnnouncementsRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/BookmarkRequest.dart';
import 'package:zebrapro_announcements/ui/base/BaseViewModel.dart';
import 'package:zebrapro_announcements/utils/enums/ViewState.dart';

class AnnouncementViewModel extends BaseViewModel {
  Future<AnnouncementsResponseModel?>? fetchAnnouncements(
      List<String> tags, bool onlyImpact, String? sentiment) async {
    AnnouncementsResponseModel? responseModel;
    setState(ViewState.busy);

    final DateTime toDate = DateTime.now();
    final DateTime fromDate =
        DateTime(toDate.year, toDate.month, toDate.day - 3);
    final DateFormat formatter = DateFormat('yyyyMMdd');
    final String toDateFormatted = formatter.format(toDate);
    final String fromDateFormatted = formatter.format(fromDate);

    AnnouncementsRequest request = AnnouncementsRequest(
        from_date: fromDateFormatted,
        to_date: toDateFormatted,
        tags: tags.join(","),
        onlyImpacts: onlyImpact,
        sentiment: sentiment);

    ApiResult<AnnouncementsResponseModel> result =
        await getDataManager().getAnnouncementsApiCall(request);
    responseModel =
        await result.when(success: (AnnouncementsResponseModel model) async {
      setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      handleGeneralApiError(error, () {});
      setState(ViewState.idle);
      return null;
    });

    return responseModel;
  }

  List<String> getFiltersTags() {
    return [
      "Acquisition",
      "New Launches",
      "Capital Sale",
      "New Releases",
      "New Orders",
      "Capital Raise",
      "Monthly Data",
      "New Appointments",
      "Agreement",
      "Bonus Issue",
      "Patent",
      "Open Offer",
      "Capital Expenditure",
      "Divestment",
      "Pledging",
      "Management",
      "Resignation",
      "Defaults",
      "Buyback",
      "Demergers",
      "IPO"
    ];
  }

  static List<Map<String, dynamic>> getFiltersTagsMap() {
    return [
      {"name": "Acquisition", "selected": true},
      {"name": "New Launches", "selected": true},
      {"name": "Capital Sale", "selected": true},
      {"name": "New Releases", "selected": true},
      {"name": "New Orders", "selected": true},
      {"name": "Capital Raise", "selected": true},
      {"name": "Monthly Data", "selected": true},
      {"name": "New Appointments", "selected": true},
      {"name": "Agreement", "selected": true},
      {"name": "Bonus Issue", "selected": true},
      {"name": "Patent", "selected": true},
      {"name": "Open Offer", "selected": true},
      {"name": "Capital Expenditure", "selected": true},
      {"name": "Divestment", "selected": true},
      {"name": "Pledging", "selected": true},
      {"name": "Management", "selected": true},
      {"name": "Resignation", "selected": true},
      {"name": "Defaults", "selected": true},
      {"name": "Buyback", "selected": true},
      {"name": "Demergers", "selected": true},
      {"name": "IPO", "selected": true},
    ];
  }

  Future<BookmarkSaveResponse?>? postBookmarkSearchResult(
      {required int resultId, required String tableName}) async {
    BookmarkSaveResponse? responseModel;
    // setState(ViewState.busy);
    BookmarkRequest request =
        BookmarkRequest(postId: resultId, tableName: tableName);

    ApiResult<BookmarkSaveResponse> result =
        await getDataManager().postBookmarkItemApiCall(request);
    responseModel =
        await result.when(success: (BookmarkSaveResponse model) async {
      // setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      // setState(ViewState.idle);
      handleGeneralApiError(error, () {});
      return null;
    });

    return responseModel;
  }

  Future<SaveImpactResponseModel?>? postImpactOnAnnouncement(
      {required int resultId, required String body}) async {
    SaveImpactResponseModel? responseModel;
    setState(ViewState.busy);
    AnnouncementImpactRequest request =
        AnnouncementImpactRequest(announcementId: resultId, impact_body: body);

    ApiResult<SaveImpactResponseModel> result =
        await getDataManager().postAnnouncementImpact(request);
    responseModel =
        await result.when(success: (SaveImpactResponseModel model) async {
      setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      setState(ViewState.idle);
      handleGeneralApiError(error, () {});
      return null;
    });

    return responseModel;
  }

  Future<SaveImpactResponseModel?>? deleteImpactOnAnnouncement(
      {required int resultId}) async {
    SaveImpactResponseModel? responseModel;
    // setState(ViewState.busy);
    ApiResult<SaveImpactResponseModel> result =
        await getDataManager().deleteAnnouncementImpact(resultId);
    responseModel =
        await result.when(success: (SaveImpactResponseModel model) async {
      // setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      // setState(ViewState.idle);
      handleGeneralApiError(error, () {});
      return null;
    });

    return responseModel;
  }

  Future<AnnouncementSaveCommentResponseModel?>? postCommentOnAnnouncement(
      {required int resultId, required String body}) async {
    AnnouncementSaveCommentResponseModel? responseModel;
    setState(ViewState.busy);
    AnnouncementCommentRequest request =
        AnnouncementCommentRequest(postId: resultId, comment_body: body);

    ApiResult<AnnouncementSaveCommentResponseModel> result =
        await getDataManager().postCommentOnAnnouncementApiCall(request);
    responseModel = await result.when(
        success: (AnnouncementSaveCommentResponseModel model) async {
      setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      setState(ViewState.idle);
      handleGeneralApiError(error, () {});
      return null;
    });

    return responseModel;
  }

  Future<AnnouncementCommentsResponseModel?>? getCommentsOfAnnouncement(
      {required int postId}) async {
    AnnouncementCommentsResponseModel? responseModel;
    // setState(ViewState.busy);
    ApiResult<AnnouncementCommentsResponseModel> result =
        await getDataManager().getAnnouncementCommentsApiCall(postId);
    responseModel = await result.when(
        success: (AnnouncementCommentsResponseModel model) async {
      // setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      // setState(ViewState.idle);
      handleGeneralApiError(error, () {});
      return null;
    });

    return responseModel;
  }
}
