import 'package:zebrapro_announcements/data/network/NetworkExceptions/NetworkExceptions.dart';
import 'package:zebrapro_announcements/data/network/model/ApiResult/ApiResult.dart';
import 'package:zebrapro_announcements/data/network/model/IndustryResponseModel.dart';
import 'package:zebrapro_announcements/ui/base/BaseViewModel.dart';
import 'package:zebrapro_announcements/utils/enums/ViewState.dart';

class StatesScannerViewModel extends BaseViewModel {
  Future<IndustryResponseModel?>? getIndustries() async {
    IndustryResponseModel? responseModel;
    setState(ViewState.busy);

    ApiResult<IndustryResponseModel> result =
        await getDataManager().getIndustryListApiCall();
    responseModel =
        await result.when(success: (IndustryResponseModel model) async {
      setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      handleGeneralApiError(error, () {});
      setState(ViewState.idle);
      return null;
    });

    return responseModel;
  }
}
