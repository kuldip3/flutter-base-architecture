import 'package:zebrapro_announcements/data/network/NetworkExceptions/NetworkExceptions.dart';
import 'package:zebrapro_announcements/data/network/model/ApiResult/ApiResult.dart';
import 'package:zebrapro_announcements/data/network/model/BookmarkSaveResponse.dart';
import 'package:zebrapro_announcements/data/network/model/IndustryResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/StatsScannerResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/requests/BookmarkRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/StatsScannerRequest.dart';
import 'package:zebrapro_announcements/ui/base/BaseViewModel.dart';
import 'package:zebrapro_announcements/utils/enums/ViewState.dart';

class StatesScannerResultViewModel extends BaseViewModel {
  Future<IndustryResponseModel?>? getIndustries() async {
    IndustryResponseModel? responseModel;
    setState(ViewState.busy);

    ApiResult<IndustryResponseModel> result =
        await getDataManager().getIndustryListApiCall();
    responseModel =
        await result.when(success: (IndustryResponseModel model) async {
      setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      handleGeneralApiError(error, () {});
      setState(ViewState.idle);
      return null;
    });

    return responseModel;
  }

  Future<StatsScannerResponseModel?>? getStatsScannerResult(
      {required String industry,
      required String query,
      required int offset}) async {
    StatsScannerResponseModel? responseModel;
    setState(ViewState.busy);
    StatsScannerRequest request =
        StatsScannerRequest(industry: industry, query: query, offset: offset);
    ApiResult<StatsScannerResponseModel> result =
        await getDataManager().getStatsScannersApiCall(request);
    responseModel =
        await result.when(success: (StatsScannerResponseModel model) async {
      setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      handleGeneralApiError(error, () {});
      setState(ViewState.idle);
      return null;
    });

    return responseModel;
  }

  Future<BookmarkSaveResponse?>? postBookmarkSearchResult(
      {required int resultId, String tableName = "resource_finder"}) async {
    BookmarkSaveResponse? responseModel;
    // setState(ViewState.busy);
    BookmarkRequest request =
        BookmarkRequest(postId: resultId, tableName: tableName);

    ApiResult<BookmarkSaveResponse> result =
        await getDataManager().postBookmarkItemApiCall(request);
    responseModel =
        await result.when(success: (BookmarkSaveResponse model) async {
      // setState(ViewState.idle);
      return model;
    }, failure: (NetworkExceptions error) {
      // setState(ViewState.idle);
      handleGeneralApiError(error, () {});
      return null;
    });

    return responseModel;
  }
}
