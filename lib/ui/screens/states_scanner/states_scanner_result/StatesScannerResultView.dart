import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:zebrapro_announcements/config/ApiConfig.dart';
import 'package:zebrapro_announcements/config/ColorConfig.dart';
import 'package:zebrapro_announcements/data/network/model/BookmarkSaveResponse.dart';
import 'package:zebrapro_announcements/data/network/model/IndustryResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/StatsScannerResponseModel.dart';
import 'package:zebrapro_announcements/ui/base/BaseView.dart';
import 'package:zebrapro_announcements/ui/screens/states_scanner/states_scanner_result/StatesScannerResultViewModel.dart';
import 'package:zebrapro_announcements/ui/widgets/AppPdfViewer.dart';

class StatesScannerResultView extends StatefulWidget {
  const StatesScannerResultView({Key? key}) : super(key: key);

  @override
  State<StatesScannerResultView> createState() => _HomeViewState();
}

class _HomeViewState extends State<StatesScannerResultView> {
  String? _argQuery = Get.arguments['query'];
  String? _argIndustry = Get.arguments['industry'];
  final _formKey = GlobalKey<FormBuilderState>();
  final _searchFieldKey = GlobalKey<FormBuilderFieldState>();
  final _industryFieldKey = GlobalKey<FormBuilderFieldState>();

  final List<String> _tags = [
    "Digital-Marketing",
    "web development",
    "Listing",
    "info ",
    "import",
    "Market Size or Reliance",
    "social",
    "shop",
    "Fashion",
  ];
  Offset? _tapPosition;
  final double _fontSize = 12;
  final List _icon = [Icons.home, Icons.language, Icons.headset];
  List<String> _industries = ["Industry not found"];
  late StatesScannerResultViewModel _model;

  List<Resp>? _stateImageList;

  @override
  Widget build(BuildContext context) {
    return BaseView<StatesScannerResultViewModel>(onModelReady: (model) async {
      _model = model;

      IndustryResponseModel? responseModel = await _model.getIndustries();
      if (responseModel != null &&
          responseModel.data != null &&
          responseModel.data!.resp != null) {
        setState(() {
          _industries = responseModel.data!.resp!;
        });
      }

      if (_argQuery != null && _argIndustry != null) {
        await fetchStatScannerResults(
            query: _argQuery!, industry: _argIndustry!, offset: 0);
      }
    }, builder: (BuildContext context, model, Widget? child) {
      return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text(
            "Stats Scanner",
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          iconTheme: const IconThemeData(color: Colors.black),
        ),
        body: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20, right: 16, left: 16),
                child: FormBuilderDropdown(
                  key: _industryFieldKey,
                  name: 'drop_down_value',
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: ColorConfig.darkGrey, width: 2.0),
                        borderRadius: BorderRadius.all(Radius.circular(12))),
                    labelText: 'Select Industry',
                    hintText: 'Select Industry',
                  ),
                  initialValue: _argIndustry,
                  onChanged: (industry) async {
                    await fetchStatScannerResults(
                        industry: industry!.toString(),
                        query: _argQuery!,
                        offset: 0);
                    _argIndustry = industry.toString();
                  },
                  items: _industries
                      .map((val) => DropdownMenuItem(
                            alignment: AlignmentDirectional.centerStart,
                            value: val,
                            child: Text(val),
                          ))
                      .toList(),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(left: 16.0, right: 16.0, top: 14),
                child: FormBuilderTextField(
                  key: _searchFieldKey,
                  name: 'search',
                  initialValue: _argQuery,
                  onSubmitted: (str) async {
                    if (str != null && str != "") {
                      await fetchStatScannerResults(
                          industry: _argIndustry!, query: str, offset: 0);
                      _argQuery = str.toString();
                    }
                  },
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.search,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(14)),
                      label: const Text("Enter Keyword"),
                      suffixIcon: const Icon(
                        Icons.search,
                        color: Colors.grey,
                      )),
                  validator: FormBuilderValidators.compose(
                      [FormBuilderValidators.required()]),
                ),
              ),
              _stateImageList != null
                  ? ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: _stateImageList!.length,
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: () {
                            openBottomSheet(_stateImageList![index]);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 16, right: 16, top: 14, bottom: 4),
                            child: Card(
                              elevation: 2,
                              shadowColor: Colors.grey,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12)),
                              child: Image(
                                  image: NetworkImage(
                                      ApiConfig.SECURE_IMAGE_URL +
                                          _stateImageList![index].imagePath!)),
                            ),
                          ),
                        );
                      },
                    )
                  : Container()
            ],
          ),
        ),
      );
    });
  }

  Future<void> fetchStatScannerResults(
      {required String industry,
      required String query,
      required int offset}) async {
    StatsScannerResponseModel? stateImageList = await _model
        .getStatsScannerResult(industry: industry, query: query, offset: 0);
    if (stateImageList != null && stateImageList.data != null) {
      setState(() {
        _stateImageList = stateImageList.data!.resp;
      });
    }
  }

  GestureDetector buildTagPill(int index, String item) {
    return GestureDetector(
      onTapDown: (details) => _tapPosition = details.globalPosition,
      child: ItemTags(
        index: index,
        pressEnabled: true,
        title: item,
        elevation: 0,
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        key: Key(index.toString()),
        color: Colors.white,
        activeColor: ColorConfig.lightAccentColor,
        active: true,
        textActiveColor: ColorConfig.accentColor,
        textColor: ColorConfig.accentColor,
        border: Border.all(color: ColorConfig.lightAccentColor),
        // removeButton: ItemTagsRemoveButton(
        //   backgroundColor: ColorConfig.accentColor,
        //   onRemoved: () {
        //     setState(() {
        //       _tags.removeAt(index);
        //     });
        //     return true;
        //   },
        // ),
        textScaleFactor: utf8.encode(item.substring(0, 1)).length > 2 ? 0.8 : 1,
        textStyle: TextStyle(fontSize: _fontSize, fontWeight: FontWeight.w500),
      ),
    );
  }

  TagsTextField get _textField {
    return TagsTextField(
      autofocus: false,
      width: 152,
      textStyle: TextStyle(fontSize: _fontSize, height: 0.0
          //height: 1
          ),
      inputDecoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(22),
          borderSide: BorderSide(color: ColorConfig.accentColor),
        ),
      ),
      enabled: true,
      constraintSuggestion: true,
      onSubmitted: (String str) {
        setState(() {
          _tags.add(str);
        });
      },
    );
  }

  void openBottomSheet(Resp resp) {
    int saved = resp.save!;
    Get.bottomSheet(
      StatefulBuilder(builder: (context, setState) {
        return Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          alignment: WrapAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 3,
                width: 72,
                color: Colors.white,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: Card(
                elevation: 2,
                shadowColor: Colors.grey,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12)),
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Image(
                      image: NetworkImage(
                          ApiConfig.SECURE_IMAGE_URL + resp.imagePath!)),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 80.0, left: 16, right: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    resp.displayLink!,
                    style: TextStyle(color: Colors.white),
                  ),
                  const SizedBox(
                    width: 22,
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: InkWell(
                        onTap: () async {
                          BookmarkSaveResponse? bookmarked =
                              await _model.postBookmarkSearchResult(
                                  resultId: resp.id!,
                                  tableName: resp.tableName!);
                          if (bookmarked != null && bookmarked.data != null) {
                            setState(() {
                              saved = bookmarked.data!.favourite! ? 1 : 0;
                            });
                          }
                        },
                        child: Container(
                          height: 34,
                          decoration: BoxDecoration(
                              color: saved == 1 ? Colors.white : Colors.black,
                              borderRadius: BorderRadius.circular(6),
                              border:
                                  Border.all(color: Colors.white, width: 1)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                "assets/svg/add_bookmark.svg",
                                height: 18,
                                color: saved == 1 ? Colors.black : Colors.white,
                              ),
                              const SizedBox(
                                width: 6,
                              ),
                              Text(
                                saved == 1 ? "Saved" : "Save",
                                style: TextStyle(
                                  color:
                                      saved == 1 ? Colors.black : Colors.white,
                                  fontSize: 12,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: InkWell(
                        onTap: () {
                          Get.to(() => AppPdfViewer(
                                pdfUrl: resp.pdfLink!,
                                title: resp.keyword!,
                              ));
                        },
                        child: Container(
                          height: 34,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(6),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [
                              Text(
                                "View",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 12),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        );
      }),
      isScrollControlled: true,
      backgroundColor: Colors.black,
      elevation: 0,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(14.0), topRight: Radius.circular(14.0))),
    );
  }
}
