import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:zebrapro_announcements/config/ColorConfig.dart';
import 'package:zebrapro_announcements/config/StyleConfig.dart';
import 'package:zebrapro_announcements/data/network/model/IndustryResponseModel.dart';
import 'package:zebrapro_announcements/routes/Routes.dart';
import 'package:zebrapro_announcements/ui/base/BaseView.dart';
import 'package:zebrapro_announcements/utils/ViewUtils.dart';

import 'StatesScannerViewModel.dart';

class StatesScannerView extends StatefulWidget {
  const StatesScannerView({Key? key}) : super(key: key);

  @override
  State<StatesScannerView> createState() => _HomeViewState();
}

class _HomeViewState extends State<StatesScannerView> {
  final _formKey = GlobalKey<FormBuilderState>();
  final _searchFieldKey = GlobalKey<FormBuilderFieldState>();
  final _industryFieldKey = GlobalKey<FormBuilderFieldState>();

  final List<String> _tags = [
    "Digital-Marketing",
    "web development",
    "Listing",
    "info ",
    "import",
    "Market Size or Reliance",
    "social",
    "shop",
    "Fashion",
  ];
  Offset? _tapPosition;
  final double _fontSize = 12;
  final List _icon = [Icons.home, Icons.language, Icons.headset];
  late StatesScannerViewModel _model;

  List<String> _industries = ["Industry not found"];

  @override
  Widget build(BuildContext context) {
    return BaseView<StatesScannerViewModel>(onModelReady: (model) async {
      _model = model;

      IndustryResponseModel? responseModel = await _model.getIndustries();
      if (responseModel != null &&
          responseModel.data != null &&
          responseModel.data!.resp != null) {
        setState(() {
          _industries = responseModel.data!.resp!;
        });
      }
    }, builder: (BuildContext context, model, Widget? child) {
      return FormBuilder(
        key: _formKey,
        child: Scaffold(
          body: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                /*const SizedBox(
                      height: 52,
                    ),*/
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: SvgPicture.asset(
                    'assets/svg/states_scanner.svg',
                    height: ViewUtils.getHeightPercent(context, 20),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.only(
                      left: 16.0, right: 16, top: 32, bottom: 4),
                  child: Text(
                    "State Scanner",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 16.0, right: 16, top: 4, bottom: 8),
                  child: Text(
                    "Find infographic around any given keyword",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.grey.shade700, fontSize: 14),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 16,
                    right: 16,
                    top: 16,
                  ),
                  child: FormBuilderDropdown(
                    key: _industryFieldKey,
                    name: 'drop_down_value',
                    decoration:
                        StyleConfig.getInputDecoration('Select Industry', null),
                    items: _industries
                        .map((val) => DropdownMenuItem(
                              alignment: AlignmentDirectional.centerStart,
                              value: val,
                              child: Text(val),
                            ))
                        .toList(),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 16.0, right: 16.0, top: 14),
                  child: FormBuilderTextField(
                    key: _searchFieldKey,
                    name: 'search',
                    onSubmitted: (str) {
                      if (str != null && str != "") {
                        Get.toNamed(RouteClass.getStatesScannerResultRoute(),
                            arguments: {
                              "query": str,
                              "industry": _industryFieldKey.currentState!.value
                            });
                      }
                    },
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.search,
                    decoration: StyleConfig.getInputDecoration(
                        'Enter Keyword',
                        IconButton(
                          onPressed: () {
                            if (_searchFieldKey.currentState != null &&
                                _searchFieldKey.currentState!.value != null &&
                                _industryFieldKey.currentState != null &&
                                _industryFieldKey.currentState!.value != null) {
                              Get.toNamed(
                                  RouteClass.getStatesScannerResultRoute(),
                                  arguments: {
                                    "query":
                                        _searchFieldKey.currentState!.value,
                                    "industry":
                                        _industryFieldKey.currentState!.value
                                  });
                            }
                          },
                          icon: const Icon(
                            Icons.search,
                            color: Colors.grey,
                            size: 34,
                          ),
                        )),
                    validator: FormBuilderValidators.compose(
                        [FormBuilderValidators.required()]),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 16.0, right: 16, top: 16, bottom: 4),
                  child: Text(
                    "Suggested Keyword",
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 16,
                        color: ColorConfig.accentColor),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 16.0, right: 16, bottom: 4),
                  child: Text(
                    "Popular Keyword to Use",
                    style: TextStyle(color: Colors.grey.shade700, fontSize: 12),
                  ),
                ),
                const SizedBox(
                  height: 22,
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 16, right: 16, bottom: 4),
                  child: Tags(
                    alignment: WrapAlignment.start,
                    //symmetry: true,
                    key: const Key("1"),
                    columns: 3,
                    textDirection: TextDirection.ltr,
                    itemCount: _tags.length,
                    itemBuilder: (index) {
                      final item = _tags[index];

                      return buildTagPill(index, item);
                    },
                  ),
                ),
                const SizedBox(
                  height: 12,
                ),
              ],
            ),
          ),
          bottomNavigationBar: Padding(
            padding: const EdgeInsets.all(16.0),
            child: InkWell(
              onTap: () {
                if (_searchFieldKey.currentState != null &&
                    _searchFieldKey.currentState!.value != null) {
                  Get.toNamed(RouteClass.getStatesScannerResultRoute(),
                      arguments: {
                        "query": _searchFieldKey.currentState!.value,
                        "industry": _industryFieldKey.currentState!.value
                      });
                }
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 56,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: ColorConfig.accentColor,
                    borderRadius: const BorderRadius.all(Radius.circular(12))),
                child: const Text(
                  "Find Reports",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    });
  }

  GestureDetector buildTagPill(int index, String item) {
    return GestureDetector(
      onTapDown: (details) => _tapPosition = details.globalPosition,
      child: ItemTags(
        index: index,
        pressEnabled: true,
        title: item,
        elevation: 0,
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        key: Key(index.toString()),
        color: Colors.white,
        activeColor: ColorConfig.lightAccentColor,
        active: true,
        textActiveColor: ColorConfig.accentColor,
        textColor: ColorConfig.accentColor,
        border: Border.all(color: ColorConfig.lightAccentColor),
        // removeButton: ItemTagsRemoveButton(
        //   backgroundColor: ColorConfig.accentColor,
        //   onRemoved: () {
        //     setState(() {
        //       _tags.removeAt(index);
        //     });
        //     return true;
        //   },
        // ),
        textScaleFactor: utf8.encode(item.substring(0, 1)).length > 2 ? 0.8 : 1,
        textStyle: TextStyle(fontSize: _fontSize, fontWeight: FontWeight.w500),
      ),
    );
  }

  TagsTextField get _textField {
    return TagsTextField(
      autofocus: false,
      width: 152,
      textStyle: TextStyle(fontSize: _fontSize, height: 0.0
          //height: 1
          ),
      inputDecoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(22),
          borderSide: BorderSide(color: ColorConfig.accentColor),
        ),
      ),
      enabled: true,
      constraintSuggestion: true,
      onSubmitted: (String str) {
        setState(() {
          _tags.add(str);
        });
      },
    );
  }
}
