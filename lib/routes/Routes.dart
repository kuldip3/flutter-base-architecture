import 'package:get/get.dart';
import 'package:zebrapro_announcements/ui/screens/home/HomeView.dart';
import 'package:zebrapro_announcements/ui/screens/login/LoginView.dart';
import 'package:zebrapro_announcements/ui/screens/report_search/report_results/ReportSearchResultView.dart';
import 'package:zebrapro_announcements/ui/screens/startup/StartupView.dart';
import 'package:zebrapro_announcements/ui/screens/states_scanner/StatesScannerView.dart';
import 'package:zebrapro_announcements/ui/screens/states_scanner/states_scanner_result/StatesScannerResultView.dart';

class RouteClass {
  static const String _splash = '/';
  static const String _login = '/login';
  static const String _home = '/home';
  static const String _announcements = '/announcements';
  static const String _reportSearch = '/report-search';
  static const String _statesScanner = '/states-scanner';
  static const String _statesScannerResult = '/states-scanner-result';

  static String getSplashRoute() => _splash;

  static String getLoginRoute() => _login;

  static String getHomeRoute() => _home;

  static String getAnnouncementsRoute() => _announcements;

  static String getReportSearchRoute() => _reportSearch;
  static String getStatesScannerRoute() => _statesScanner;
  static String getStatesScannerResultRoute() => _statesScannerResult;

  static List<GetPage> routes = [
    GetPage(name: _splash, page: () => const StartupView()),
    GetPage(name: _login, page: () => const LoginView()),
    GetPage(name: _home, page: () => const HomeView()),
    GetPage(name: _reportSearch, page: () => const ReportSearchResultView()),
    GetPage(name: _statesScanner, page: () => const StatesScannerView()),
    GetPage(name: _statesScannerResult, page: () => const StatesScannerResultView()),
  ];
}
