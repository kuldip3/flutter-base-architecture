import 'package:get/get.dart';
import 'package:zebrapro_announcements/data/network/ApiEndPoint.dart';
import 'package:zebrapro_announcements/data/network/ApiHelper.dart';
import 'package:zebrapro_announcements/data/network/Client.dart';
import 'package:zebrapro_announcements/data/network/NetworkExceptions/NetworkExceptions.dart';
import 'package:zebrapro_announcements/data/network/model/AnnouncementCommentsResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/AnnouncementSaveCommentResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/AnnouncementsResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ApiResult/ApiResult.dart';
import 'package:zebrapro_announcements/data/network/model/BookmarkSaveResponse.dart';
import 'package:zebrapro_announcements/data/network/model/ChannelListResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ChannelSubscribeResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ChannelSubscriptionListResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/GeneralResponse.dart';
import 'package:zebrapro_announcements/data/network/model/IndustryResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/LoginRequestModel.dart';
import 'package:zebrapro_announcements/data/network/model/LoginResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ProfileDataResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ReportSearchProcessStatusResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ReportSearchResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ReportSearchResultLikeResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/SaveImpactResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/StatsScannerResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/VerifyTokenResponse.dart';
import 'package:zebrapro_announcements/data/network/model/requests/AnnouncementCommentRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/AnnouncementImpactRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/AnnouncementsRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/BookmarkRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/ChannelSubscribeRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/FirebaseTokenRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/NotificationStatusRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/ReportSearchProcessingStatusRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/ReportSearchRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/ReportSearchResultLikeRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/SendNotificationRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/StatsScannerRequest.dart';

class AppApiHelper implements ApiHelper {
  Client _mClient = Get.find<Client>();

  AppApiHelper() {
    _mClient = _mClient.builder();
  }

  @override
  Future<ApiResult<LoginResponseModel>> loginApiCall(
      LoginRequestModel loginRequestModel) async {
    try {
      print(loginRequestModel.toJson());
      var _response = await _mClient.workstationUrl().build().post(
            ApiEndPoints.ENDPOINT_LOGIN,
            data: loginRequestModel.toJson(),
          );
      print(_response);
      LoginResponseModel _model = LoginResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<bool>> logoutApiCall() async {
    try {
      var _response = await _mClient
          .workstationUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_LOGOUT);
      return const ApiResult.success(data: true);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<VerifyTokenResponse>> verifyTokenApiCall() async {
    try {
      var _response = await _mClient
          .workstationUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_VERIFY);
      VerifyTokenResponse _model = VerifyTokenResponse.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<AnnouncementsResponseModel>> getAnnouncementsApiCall(
      AnnouncementsRequest request) async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .get(ApiEndPoints.ENDPOINT_ANNOUNCEMENTS,
              queryParameters: request.toJson());
      AnnouncementsResponseModel _model =
          AnnouncementsResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<ReportSearchResponseModel>> getReportSearchApiCall(
      ReportSearchRequest request) async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_REPORT_SEARCH, data: request.toJson());
      ReportSearchResponseModel _model =
          ReportSearchResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<ReportSearchProcessStatusResponseModel>>
      getReportSearchProcessingStatusApiCall(
          ReportSearchProcessingStatusRequest request) async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_REPORT_SEARCH_PROCESSING_STATUS,
              data: request.toJson());
      ReportSearchProcessStatusResponseModel _model =
          ReportSearchProcessStatusResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<ReportSearchResultLikeResponseModel>>
      postLikeToReportSearchResultApiCall(
          ReportSearchResultLikeRequest request) async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_REPORT_SEARCH_LIKE,
              data: request.toJson());
      ReportSearchResultLikeResponseModel _model =
          ReportSearchResultLikeResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<BookmarkSaveResponse>> postBookmarkItemApiCall(
      BookmarkRequest request) async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_BOOKMARK_ITEM, data: request.toJson());
      BookmarkSaveResponse _model =
          BookmarkSaveResponse.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<IndustryResponseModel>> getIndustryListApiCall() async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_STATS_SCANNER_INDUSTRY_LIST);
      IndustryResponseModel _model =
          IndustryResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<StatsScannerResponseModel>> getStatsScannersApiCall(
      StatsScannerRequest request) async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .get(ApiEndPoints.ENDPOINT_STATS_SEARCH,
              queryParameters: request.toJson());
      StatsScannerResponseModel _model =
          StatsScannerResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<ProfileDataResponseModel>> getProfileData() async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_PROFILE_DATA);
      ProfileDataResponseModel _model =
          ProfileDataResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<SaveImpactResponseModel>> deleteAnnouncementImpact(
      int impactId) async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_ANNOUNCEMENTS_IMPACT_DELETE,
              data: {'impact_id': impactId});
      SaveImpactResponseModel _model =
          SaveImpactResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<AnnouncementCommentsResponseModel>>
      getAnnouncementCommentsApiCall(int postId) async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .get(ApiEndPoints.ENDPOINT_FETCH_ANNOUNCEMENT_COMMENT,
              queryParameters: {'post_id': postId});
      AnnouncementCommentsResponseModel _model =
          AnnouncementCommentsResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<SaveImpactResponseModel>> postAnnouncementImpact(
      AnnouncementImpactRequest request) async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_ANNOUNCEMENTS_IMPACT_SAVE,
              data: request.toJson());
      SaveImpactResponseModel _model =
          SaveImpactResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<AnnouncementSaveCommentResponseModel>>
      postCommentOnAnnouncementApiCall(
          AnnouncementCommentRequest request) async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_SAVE_ANNOUNCEMENT_COMMENT,
              data: request.toJson());
      AnnouncementSaveCommentResponseModel _model =
          AnnouncementSaveCommentResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<ChannelListResponseModel>> getChannelListApiCall() async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_CHANNELS);
      ChannelListResponseModel _model =
          ChannelListResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<ChannelSubscriptionListResponseModel>>
      getChannelSubscriptionsApiCall() async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_CHANNELS_SUBSCRIBTIONS);
      ChannelSubscriptionListResponseModel _model =
          ChannelSubscriptionListResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<GeneralResponse>> postFirebaseTokenApiCall(
      FirebaseTokenRequest request) async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_FIREBASE_TOKEN, data: request.toJson());
      GeneralResponse _model = GeneralResponse.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<GeneralResponse>> postNotificationStatus(
      NotificationStatusRequest request) async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_NOTIFICATIONS_STATUS,
              data: request.toJson());
      GeneralResponse _model = GeneralResponse.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<GeneralResponse>> postSendNotification(
      SendNotificationRequest request) async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_NOTIFICATIONS_SEND,
              data: request.toJson());
      GeneralResponse _model = GeneralResponse.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }

  @override
  Future<ApiResult<ChannelSubscribeResponseModel>>
      subscribeToNotificationChannel(ChannelSubscribeRequest request) async {
    try {
      var _response = await _mClient
          .appUrl()
          .setProtectedApiHeader()
          .setUrlEncoded()
          .build()
          .post(ApiEndPoints.ENDPOINT_CHANNELS_SUBSCRIBE,
              data: request.toJson());
      ChannelSubscribeResponseModel _model =
          ChannelSubscribeResponseModel.fromJson(_response.data);
      return ApiResult.success(data: _model);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }
}
