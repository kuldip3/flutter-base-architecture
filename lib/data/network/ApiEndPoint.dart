class ApiEndPoints {
  static const String ENDPOINT_LOGIN = "/login";
  static const String ENDPOINT_LOGOUT = "/logout";
  static const String ENDPOINT_VERIFY = "/verify";
  static const String ENDPOINT_ANNOUNCEMENTS = "/get-filtered-announcements";
  static const String ENDPOINT_REPORT_SEARCH = "/resource-get-res";
  static const String ENDPOINT_REPORT_SEARCH_PROCESSING_STATUS = "/resource-v1";
  static const String ENDPOINT_REPORT_SEARCH_LIKE = "/resource_finder/like";
  static const String ENDPOINT_STATS_SCANNER_INDUSTRY_LIST =
      "/stats-scanner-industry-list";
  static const String ENDPOINT_STATS_SEARCH = "/get-stat-images";

  static const String ENDPOINT_BOOKMARK_ITEM = "/save-item";
  static const String ENDPOINT_PROFILE_DATA = "/profile-details";

  static const String ENDPOINT_SAVE_ANNOUNCEMENT_COMMENT =
      "/save-announcement-comment";
  static const String ENDPOINT_FETCH_ANNOUNCEMENT_COMMENT =
      "/fetch-announcement-comment";
  static const String ENDPOINT_ANNOUNCEMENTS_IMPACT_DELETE =
      "/announcements/impact/delete";
  static const String ENDPOINT_ANNOUNCEMENTS_IMPACT_SAVE =
      "/announcements/impact";

  static const String ENDPOINT_FIREBASE_TOKEN = "/firebase-token";
  static const String ENDPOINT_CHANNELS = "/channels";
  static const String ENDPOINT_CHANNELS_SUBSCRIBE = "/channels/subscribe";
  static const String ENDPOINT_CHANNELS_SUBSCRIBTIONS = "/channels/subscribers";
  static const String ENDPOINT_NOTIFICATIONS_SEND = "/notifications/send";
  static const String ENDPOINT_NOTIFICATIONS_STATUS = "/notifications/status";
}
