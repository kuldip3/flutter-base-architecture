import 'dart:convert';

IndustryResponseModel industryResponseModelFromJson(String str) =>
    IndustryResponseModel.fromJson(json.decode(str));

String industryResponseModelToJson(IndustryResponseModel data) =>
    json.encode(data.toJson());

class IndustryResponseModel {
  IndustryResponseModel({
    this.data,
    this.message,
    this.success,
  });

  IndustryResponseModel.fromJson(dynamic json) {
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
    message = json['message'];
    success = json['success'];
  }

  Data? data;
  String? message;
  bool? success;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (data != null) {
      map['data'] = data?.toJson();
    }
    map['message'] = message;
    map['success'] = success;
    return map;
  }
}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));

String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.resp,
  });

  Data.fromJson(dynamic json) {
    resp = json['resp'] != null ? json['resp'].cast<String>() : [];
  }

  List<String>? resp;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['resp'] = resp;
    return map;
  }
}
