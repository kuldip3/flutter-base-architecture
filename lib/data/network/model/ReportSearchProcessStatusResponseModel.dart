import 'dart:convert';

ReportSearchProcessStatusResponseModel
    reportSearchProcessStatusResponseModelFromJson(String str) =>
        ReportSearchProcessStatusResponseModel.fromJson(json.decode(str));

String reportSearchProcessStatusResponseModelToJson(
        ReportSearchProcessStatusResponseModel data) =>
    json.encode(data.toJson());

class ReportSearchProcessStatusResponseModel {
  ReportSearchProcessStatusResponseModel({
    String? data,
    String? message,
    bool? success,
  }) {
    _data = data;
    _message = message;
    _success = success;
  }

  ReportSearchProcessStatusResponseModel.fromJson(dynamic json) {
    _data = json['data'];
    _message = json['message'];
    _success = json['success'];
  }

  String? _data;
  String? _message;
  bool? _success;

  String? get data => _data;

  String? get message => _message;

  bool? get success => _success;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['data'] = _data;
    map['message'] = _message;
    map['success'] = _success;
    return map;
  }
}
