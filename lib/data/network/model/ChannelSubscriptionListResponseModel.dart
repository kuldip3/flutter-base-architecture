import 'dart:convert';

ChannelSubscriptionListResponseModel
    channelSubscriptionListResponseModelFromJson(String str) =>
        ChannelSubscriptionListResponseModel.fromJson(json.decode(str));

String channelSubscriptionListResponseModelToJson(
        ChannelSubscriptionListResponseModel data) =>
    json.encode(data.toJson());

class ChannelSubscriptionListResponseModel {
  ChannelSubscriptionListResponseModel({
    this.data,
    this.message,
    this.success,
  });

  ChannelSubscriptionListResponseModel.fromJson(dynamic json) {
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
    message = json['message'];
    success = json['success'];
  }

  Data? data;
  String? message;
  bool? success;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (data != null) {
      map['data'] = data?.toJson();
    }
    map['message'] = message;
    map['success'] = success;
    return map;
  }
}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));

String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.myChannelsSub,
  });

  Data.fromJson(dynamic json) {
    if (json['my_channels_sub'] != null) {
      myChannelsSub = [];
      json['my_channels_sub'].forEach((v) {
        myChannelsSub?.add(MyChannelsSub.fromJson(v));
      });
    }
  }

  List<MyChannelsSub>? myChannelsSub;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (myChannelsSub != null) {
      map['my_channels_sub'] = myChannelsSub?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

MyChannelsSub myChannelsSubFromJson(String str) =>
    MyChannelsSub.fromJson(json.decode(str));

String myChannelsSubToJson(MyChannelsSub data) => json.encode(data.toJson());

class MyChannelsSub {
  MyChannelsSub({
    this.channelId,
    this.channelName,
    this.channelSlug,
    this.isSubscribed,
  });

  MyChannelsSub.fromJson(dynamic json) {
    channelId = json['channel_id'];
    channelName = json['channel_name'];
    channelSlug = json['channel_slug'];
    isSubscribed = json['is_subscribed'];
  }

  int? channelId;
  String? channelName;
  String? channelSlug;
  bool? isSubscribed;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['channel_id'] = channelId;
    map['channel_name'] = channelName;
    map['channel_slug'] = channelSlug;
    map['is_subscribed'] = isSubscribed;
    return map;
  }
}
