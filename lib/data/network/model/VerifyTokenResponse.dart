import 'dart:convert';

VerifyTokenResponse verifyTokenResponseFromJson(String str) =>
    VerifyTokenResponse.fromJson(json.decode(str));

String verifyTokenResponseToJson(VerifyTokenResponse data) =>
    json.encode(data.toJson());

class VerifyTokenResponse {
  VerifyTokenResponse({
    bool? success,
    String? message,
    VerifyTokenResponseData? verifyTokenResponseData,
  }) {
    _success = success;
    _message = message;
    _data = verifyTokenResponseData;
  }

  VerifyTokenResponse.fromJson(dynamic json) {
    _success = json['success'];
    _message = json['message'];
    _data = json['data'] != null
        ? VerifyTokenResponseData.fromJson(json['data'])
        : null;
  }

  bool? _success;
  String? _message;
  VerifyTokenResponseData? _data;

  VerifyTokenResponse copyWith({
    bool? success,
    String? message,
    VerifyTokenResponseData? verifyTokenResponseData,
  }) =>
      VerifyTokenResponse(
        success: success ?? _success,
        message: message ?? _message,
        verifyTokenResponseData: verifyTokenResponseData ?? _data,
      );

  bool? get success => _success;

  String? get message => _message;

  VerifyTokenResponseData? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = _success;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }
}

VerifyTokenResponseData verifyTokenResponseDataFromJson(String str) =>
    VerifyTokenResponseData.fromJson(json.decode(str));

String verifyTokenResponseDataToJson(VerifyTokenResponseData data) =>
    json.encode(data.toJson());

class VerifyTokenResponseData {
  VerifyTokenResponseData({
    int? id,
    int? roleId,
    String? name,
    String? username,
    String? email,
    String? phone,
    bool? isActive,
    String? createdAt,
    int? pmsCompanyId,
  }) {
    _id = id;
    _roleId = roleId;
    _name = name;
    _username = username;
    _email = email;
    _phone = phone;
    _isActive = isActive;
    _createdAt = createdAt;
    _pmsCompanyId = pmsCompanyId;
  }

  VerifyTokenResponseData.fromJson(dynamic json) {
    _id = json['id'];
    _roleId = json['role_id'];
    _name = json['name'];
    _username = json['username'];
    _email = json['email'];
    _phone = json['phone'];
    _isActive = json['is_active'];
    _createdAt = json['created_at'];
    _pmsCompanyId = json['pms_company_id'];
  }

  int? _id;
  int? _roleId;
  String? _name;
  String? _username;
  String? _email;
  String? _phone;
  bool? _isActive;
  String? _createdAt;
  int? _pmsCompanyId;

  VerifyTokenResponseData copyWith({
    int? id,
    int? roleId,
    String? name,
    String? username,
    String? email,
    String? phone,
    bool? isActive,
    String? createdAt,
    int? pmsCompanyId,
  }) =>
      VerifyTokenResponseData(
        id: id ?? _id,
        roleId: roleId ?? _roleId,
        name: name ?? _name,
        username: username ?? _username,
        email: email ?? _email,
        phone: phone ?? _phone,
        isActive: isActive ?? _isActive,
        createdAt: createdAt ?? _createdAt,
        pmsCompanyId: pmsCompanyId ?? _pmsCompanyId,
      );

  int? get id => _id;

  int? get roleId => _roleId;

  String? get name => _name;

  String? get username => _username;

  String? get email => _email;

  String? get phone => _phone;

  bool? get isActive => _isActive;

  String? get createdAt => _createdAt;

  int? get pmsCompanyId => _pmsCompanyId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['role_id'] = _roleId;
    map['name'] = _name;
    map['username'] = _username;
    map['email'] = _email;
    map['phone'] = _phone;
    map['is_active'] = _isActive;
    map['created_at'] = _createdAt;
    map['pms_company_id'] = _pmsCompanyId;
    return map;
  }
}
