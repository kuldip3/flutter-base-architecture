import 'dart:convert';

AnnouncementCommentsResponseModel announcementCommentsResponseModelFromJson(
        String str) =>
    AnnouncementCommentsResponseModel.fromJson(json.decode(str));

String announcementCommentsResponseModelToJson(
        AnnouncementCommentsResponseModel data) =>
    json.encode(data.toJson());

class AnnouncementCommentsResponseModel {
  AnnouncementCommentsResponseModel({
    this.data,
    this.message,
    this.success,
  });

  AnnouncementCommentsResponseModel.fromJson(dynamic json) {
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(Data.fromJson(v));
      });
    }
    message = json['message'];
    success = json['success'];
  }

  List<Data>? data;
  String? message;
  bool? success;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    map['message'] = message;
    map['success'] = success;
    return map;
  }
}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));

String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.commentBody,
    this.commentedTime,
    this.createdAt,
    this.id,
    this.parentId,
    this.postId,
    this.user,
    this.userId,
  });

  Data.fromJson(dynamic json) {
    commentBody = json['comment_body'];
    commentedTime = json['commented_time'];
    createdAt = json['created_at'];
    id = json['id'];
    parentId = json['parent_id'];
    postId = json['post_id'];
    user = json['user'] != null ? User.fromJson(json['user']) : null;
    userId = json['user_id'];
  }

  String? commentBody;
  String? commentedTime;
  String? createdAt;
  int? id;
  String? parentId;
  int? postId;
  User? user;
  int? userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['comment_body'] = commentBody;
    map['commented_time'] = commentedTime;
    map['created_at'] = createdAt;
    map['id'] = id;
    map['parent_id'] = parentId;
    map['post_id'] = postId;
    if (user != null) {
      map['user'] = user?.toJson();
    }
    map['user_id'] = userId;
    return map;
  }
}

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  User({
    this.createdAt,
    this.email,
    this.id,
    this.isActive,
    this.name,
    this.phone,
    this.pmsCompanyId,
    this.roleId,
    this.username,
  });

  User.fromJson(dynamic json) {
    createdAt = json['created_at'];
    email = json['email'];
    id = json['id'];
    isActive = json['is_active'];
    name = json['name'];
    phone = json['phone'];
    pmsCompanyId = json['pms_company_id'];
    roleId = json['role_id'];
    username = json['username'];
  }

  String? createdAt;
  String? email;
  int? id;
  bool? isActive;
  String? name;
  String? phone;
  int? pmsCompanyId;
  int? roleId;
  String? username;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['created_at'] = createdAt;
    map['email'] = email;
    map['id'] = id;
    map['is_active'] = isActive;
    map['name'] = name;
    map['phone'] = phone;
    map['pms_company_id'] = pmsCompanyId;
    map['role_id'] = roleId;
    map['username'] = username;
    return map;
  }
}
