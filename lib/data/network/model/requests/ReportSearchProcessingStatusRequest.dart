class ReportSearchProcessingStatusRequest {
  ReportSearchProcessingStatusRequest({
    required this.keyword,
    required this.preferencelist,
  });

  String keyword;
  String preferencelist;

  factory ReportSearchProcessingStatusRequest.fromJson(
          Map<String, dynamic> json) =>
      ReportSearchProcessingStatusRequest(
        keyword: json["keyword"],
        preferencelist: json["preferencelist"],
      );

  Map<String, dynamic> toJson() => {
        "keyword": keyword,
        "preferencelist": preferencelist,
      };
}
