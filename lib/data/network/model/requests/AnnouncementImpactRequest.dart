class AnnouncementImpactRequest {
  AnnouncementImpactRequest({
    required this.announcementId,
    required this.impact_body,
  });

  int announcementId;
  String impact_body;

  factory AnnouncementImpactRequest.fromJson(Map<String, dynamic> json) =>
      AnnouncementImpactRequest(
        announcementId: json["announcement_id"],
        impact_body: json["impact_body"],
      );

  Map<String, dynamic> toJson() => {
        "announcement_id": announcementId,
        "impact_body": impact_body,
      };
}
