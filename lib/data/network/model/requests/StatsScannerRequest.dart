class StatsScannerRequest {
  StatsScannerRequest({
    required this.industry,
    required this.query,
    required this.offset,
  });

  String industry;
  String query;
  int offset;

  factory StatsScannerRequest.fromJson(Map<String, dynamic> json) =>
      StatsScannerRequest(
        industry: json["keyword"],
        query: json["adv_keyword"],
        offset: json["offset"],
      );

  Map<String, dynamic> toJson() => {
        "keyword": industry,
        "adv_keyword": query,
        "offset": offset,
      };
}
