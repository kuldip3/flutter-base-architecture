import 'dart:convert';

SendNotificationRequest sendNotificationRequestFromJson(String str) =>
    SendNotificationRequest.fromJson(json.decode(str));

String sendNotificationRequestToJson(SendNotificationRequest data) =>
    json.encode(data.toJson());

class SendNotificationRequest {
  SendNotificationRequest({
    this.title,
    this.body,
    this.channel,
    this.image,
    this.action,
  });

  SendNotificationRequest.fromJson(dynamic json) {
    title = json['title'];
    body = json['body'];
    channel = json['channel'];
    image = json['image'];
    action = json['action'];
  }

  String? title;
  String? body;
  String? channel;
  dynamic image;
  String? action;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['title'] = title;
    map['body'] = body;
    map['channel'] = channel;
    map['image'] = image;
    map['action'] = action;
    return map;
  }
}
