class AnnouncementsRequest {
  AnnouncementsRequest({
    required this.from_date,
    required this.to_date,
    required this.tags,
    required this.sentiment,
    required this.onlyImpacts,
  });

  String from_date;
  String to_date;
  String tags;
  String? sentiment;
  bool onlyImpacts;

  factory AnnouncementsRequest.fromJson(Map<String, dynamic> json) =>
      AnnouncementsRequest(
        from_date: json["from_date"],
        to_date: json["to_date"],
        tags: json["tags"],
        onlyImpacts: json["only_impacts"],
        sentiment: json["sentiment"],
      );

  Map<String, dynamic> toJson() => {
        "from_date": from_date,
        "to_date": to_date,
        "tags": tags,
        "only_impacts": onlyImpacts,
        "sentiment": sentiment,
      }..removeWhere((key, value) => key == null || value == null);
}
