import 'dart:convert';

NotificationStatusRequest notificationStatusRequestFromJson(String str) =>
    NotificationStatusRequest.fromJson(json.decode(str));

String notificationStatusRequestToJson(NotificationStatusRequest data) =>
    json.encode(data.toJson());

class NotificationStatusRequest {
  NotificationStatusRequest({
    this.notificationId,
    this.status,
  });

  NotificationStatusRequest.fromJson(dynamic json) {
    notificationId = json['notification_id'];
    status = json['status'];
  }

  int? notificationId;
  String? status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['notification_id'] = notificationId;
    map['status'] = status;
    return map;
  }
}
