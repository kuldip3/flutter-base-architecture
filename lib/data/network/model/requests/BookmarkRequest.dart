class BookmarkRequest {
  BookmarkRequest({
    required this.postId,
    required this.tableName,
  });

  int postId;
  String tableName;

  factory BookmarkRequest.fromJson(Map<String, dynamic> json) =>
      BookmarkRequest(
        postId: json["post_id"],
        tableName: json["tablename"],
      );

  Map<String, dynamic> toJson() => {
        "post_id": postId,
        "tablename": tableName,
      };
}
