class ReportSearchResultLikeRequest {
  ReportSearchResultLikeRequest({
    required this.itemId,
    this.type = "table",
    required this.like,
  });

  int itemId;
  String type;
  int like;

  factory ReportSearchResultLikeRequest.fromJson(Map<String, dynamic> json) =>
      ReportSearchResultLikeRequest(
        itemId: json["item_id"],
        type: json["type"],
        like: json["like"],
      );

  Map<String, dynamic> toJson() => {
    "item_id": itemId,
    "type": type,
    "like": like,
  };
}
