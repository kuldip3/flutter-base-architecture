class AnnouncementCommentRequest {
  AnnouncementCommentRequest({
    required this.postId,
    required this.comment_body,
  });

  int postId;
  String comment_body;

  factory AnnouncementCommentRequest.fromJson(Map<String, dynamic> json) =>
      AnnouncementCommentRequest(
        postId: json["post_id"],
        comment_body: json["comment_body"],
      );

  Map<String, dynamic> toJson() => {
        "post_id": postId,
        "comment_body": comment_body,
      };
}
