import 'dart:convert';

FirebaseTokenRequest firebaseTokenRequestFromJson(String str) =>
    FirebaseTokenRequest.fromJson(json.decode(str));

String firebaseTokenRequestToJson(FirebaseTokenRequest data) =>
    json.encode(data.toJson());

class FirebaseTokenRequest {
  FirebaseTokenRequest({
    this.token,
    this.device,
    this.deviceId,
  });

  FirebaseTokenRequest.fromJson(dynamic json) {
    token = json['token'];
    device = json['device'];
    deviceId = json['device_id'];
  }

  String? token;
  String? device;
  String? deviceId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['token'] = token;
    map['device'] = device;
    map['device_id'] = deviceId;
    return map;
  }
}
