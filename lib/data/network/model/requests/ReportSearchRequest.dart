class ReportSearchRequest {
  ReportSearchRequest({
    required this.keyword,
    required this.preferencelist,
  });

  String keyword;
  String preferencelist;

  factory ReportSearchRequest.fromJson(Map<String, dynamic> json) =>
      ReportSearchRequest(
        keyword: json["keyword"],
        preferencelist: json["preferencelist"],
      );

  Map<String, dynamic> toJson() => {
        "keyword": keyword,
        "preferencelist": preferencelist,
      };
}
