import 'dart:convert';

ChannelSubscribeRequest channelSubscribeRequestFromJson(String str) =>
    ChannelSubscribeRequest.fromJson(json.decode(str));

String channelSubscribeRequestToJson(ChannelSubscribeRequest data) =>
    json.encode(data.toJson());

class ChannelSubscribeRequest {
  ChannelSubscribeRequest({
    this.channelId,
    this.subscribe,
  });

  ChannelSubscribeRequest.fromJson(dynamic json) {
    channelId = json['channel_id'];
    subscribe = json['subscribe'];
  }

  int? channelId;
  bool? subscribe;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['channel_id'] = channelId;
    map['subscribe'] = subscribe;
    return map;
  }
}
