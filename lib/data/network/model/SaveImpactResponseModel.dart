class SaveImpactResponseModel {
  SaveImpactResponseModel({
    this.data,
    this.message,
    this.success,
  });

  SaveImpactResponseModel.fromJson(dynamic json) {
    data = json['data'];
    message = json['message'];
    success = json['success'];
  }

  String? data;
  String? message;
  bool? success;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['data'] = data;
    map['message'] = message;
    map['success'] = success;
    return map;
  }
}
