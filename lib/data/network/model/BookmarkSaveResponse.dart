import 'dart:convert';

BookmarkSaveResponse bookmarkSaveResponseFromJson(String str) =>
    BookmarkSaveResponse.fromJson(json.decode(str));

String bookmarkSaveResponseToJson(BookmarkSaveResponse data) =>
    json.encode(data.toJson());

class BookmarkSaveResponse {
  BookmarkSaveResponse({
    this.data,
    this.message,
    this.success,
  });

  BookmarkSaveResponse.fromJson(dynamic json) {
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
    message = json['message'];
    success = json['success'];
  }

  Data? data;
  String? message;
  bool? success;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (data != null) {
      map['data'] = data?.toJson();
    }
    map['message'] = message;
    map['success'] = success;
    return map;
  }
}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));

String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.favourite,
    this.id,
    this.itemId,
    this.tablename,
    this.userId,
  });

  Data.fromJson(dynamic json) {
    favourite = json['favourite'];
    id = json['id'];
    itemId = json['item_id'];
    tablename = json['tablename'];
    userId = json['user_id'];
  }

  bool? favourite;
  int? id;
  int? itemId;
  String? tablename;
  int? userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['favourite'] = favourite;
    map['id'] = id;
    map['item_id'] = itemId;
    map['tablename'] = tablename;
    map['user_id'] = userId;
    return map;
  }
}
