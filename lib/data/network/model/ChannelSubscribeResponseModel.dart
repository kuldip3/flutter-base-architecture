import 'dart:convert';

ChannelSubscribeResponseModel channelSubscribeResponseModelFromJson(
        String str) =>
    ChannelSubscribeResponseModel.fromJson(json.decode(str));

String channelSubscribeResponseModelToJson(
        ChannelSubscribeResponseModel data) =>
    json.encode(data.toJson());

class ChannelSubscribeResponseModel {
  ChannelSubscribeResponseModel({
    this.data,
    this.message,
    this.success,
  });

  ChannelSubscribeResponseModel.fromJson(dynamic json) {
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
    message = json['message'];
    success = json['success'];
  }

  Data? data;
  String? message;
  bool? success;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (data != null) {
      map['data'] = data?.toJson();
    }
    map['message'] = message;
    map['success'] = success;
    return map;
  }
}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));

String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.channelId,
    this.firebaseTokenId,
    this.id,
    this.unsubscribedAt,
    this.userId,
  });

  Data.fromJson(dynamic json) {
    channelId = json['channel_id'];
    firebaseTokenId = json['firebase_token_id'];
    id = json['id'];
    unsubscribedAt = json['unsubscribed_at'];
    userId = json['user_id'];
  }

  int? channelId;
  dynamic firebaseTokenId;
  int? id;
  dynamic unsubscribedAt;
  int? userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['channel_id'] = channelId;
    map['firebase_token_id'] = firebaseTokenId;
    map['id'] = id;
    map['unsubscribed_at'] = unsubscribedAt;
    map['user_id'] = userId;
    return map;
  }
}
