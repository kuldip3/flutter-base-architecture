import 'dart:convert';

AnnouncementsResponseModel announcementsResponseModelFromJson(String str) =>
    AnnouncementsResponseModel.fromJson(json.decode(str));

String announcementsResponseModelToJson(AnnouncementsResponseModel data) =>
    json.encode(data.toJson());

class AnnouncementsResponseModel {
  AnnouncementsResponseModel({
    this.data,
    this.message,
    this.success,
  });

  AnnouncementsResponseModel.fromJson(dynamic json) {
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
    message = json['message'];
    success = json['success'];
  }

  Data? data;
  String? message;
  bool? success;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (data != null) {
      map['data'] = data?.toJson();
    }
    map['message'] = message;
    map['success'] = success;
    return map;
  }
}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));

String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.lastUpdatedTime,
    this.result,
    this.totalCount,
  });

  Data.fromJson(dynamic json) {
    lastUpdatedTime = json['last_updated_time'];
    if (json['result'] != null) {
      result = [];
      json['result'].forEach((v) {
        result?.add(Result.fromJson(v));
      });
    }
    totalCount = json['total_count'];
  }

  String? lastUpdatedTime;
  List<Result>? result;
  int? totalCount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['last_updated_time'] = lastUpdatedTime;
    if (result != null) {
      map['result'] = result?.map((v) => v.toJson()).toList();
    }
    map['total_count'] = totalCount;
    return map;
  }
}

Result resultFromJson(String str) => Result.fromJson(json.decode(str));

String resultToJson(Result data) => json.encode(data.toJson());

class Result {
  Result({
    this.attachmentname,
    this.headline,
    this.newsid,
    this.newssub,
    this.newsdt,
    this.scripcd,
    this.slongname,
    this.sentiment,
    this.sentimentScore,
    this.tags,
    this.totalPageCnt,
    this.companyId,
    this.id,
    this.impact,
    this.impactId,
    this.industryId,
    this.logoUrl,
    this.name,
    this.pdfLink,
    this.save,
    this.strDate,
  });

  Result.fromJson(dynamic json) {
    attachmentname = json['ATTACHMENTNAME'];
    headline = json['HEADLINE'];
    newsid = json['NEWSID'];
    newssub = json['NEWSSUB'];
    newsdt = json['NEWS_DT'];
    scripcd = json['SCRIP_CD'];
    slongname = json['SLONGNAME'];
    sentiment = json['Sentiment'];
    sentimentScore = json['Sentiment_Score'];
    tags = json['Tags'];
    totalPageCnt = json['TotalPageCnt'];
    companyId = json['company_id'];
    id = json['id'];
    impact = json['impact'];
    impactId = json['impact_id'];
    industryId = json['industry_id'];
    logoUrl = json['logo_url'];
    name = json['name'];
    pdfLink = json['pdf_link'];
    save = json['save'];
    strDate = json['strDate'];
  }

  String? attachmentname;
  String? headline;
  String? newsid;
  String? newssub;
  String? newsdt;
  int? scripcd;
  String? slongname;
  String? sentiment;
  int? sentimentScore;
  String? tags;
  int? totalPageCnt;
  String? companyId;
  int? id;
  String? impact;
  int? impactId;
  int? industryId;
  String? logoUrl;
  String? name;
  String? pdfLink;
  int? save;
  String? strDate;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['ATTACHMENTNAME'] = attachmentname;
    map['HEADLINE'] = headline;
    map['NEWSID'] = newsid;
    map['NEWSSUB'] = newssub;
    map['NEWS_DT'] = newsdt;
    map['SCRIP_CD'] = scripcd;
    map['SLONGNAME'] = slongname;
    map['Sentiment'] = sentiment;
    map['Sentiment_Score'] = sentimentScore;
    map['Tags'] = tags;
    map['TotalPageCnt'] = totalPageCnt;
    map['company_id'] = companyId;
    map['id'] = id;
    map['impact'] = impact;
    map['impact_id'] = impactId;
    map['industry_id'] = industryId;
    map['logo_url'] = logoUrl;
    map['name'] = name;
    map['pdf_link'] = pdfLink;
    map['save'] = save;
    map['strDate'] = strDate;
    return map;
  }
}
