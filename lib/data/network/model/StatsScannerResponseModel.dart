import 'dart:convert';

StatsScannerResponseModel statsScannerResponseModelFromJson(String str) =>
    StatsScannerResponseModel.fromJson(json.decode(str));

String statsScannerResponseModelToJson(StatsScannerResponseModel data) =>
    json.encode(data.toJson());

class StatsScannerResponseModel {
  StatsScannerResponseModel({
    this.data,
    this.message,
    this.success,
  });

  StatsScannerResponseModel.fromJson(dynamic json) {
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
    message = json['message'];
    success = json['success'];
  }

  Data? data;
  String? message;
  bool? success;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (data != null) {
      map['data'] = data?.toJson();
    }
    map['message'] = message;
    map['success'] = success;
    return map;
  }
}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));

String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.resp,
  });

  Data.fromJson(dynamic json) {
    if (json['resp'] != null) {
      resp = [];
      json['resp'].forEach((v) {
        resp?.add(Resp.fromJson(v));
      });
    }
  }

  List<Resp>? resp;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (resp != null) {
      map['resp'] = resp?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

Resp respFromJson(String str) => Resp.fromJson(json.decode(str));

String respToJson(Resp data) => json.encode(data.toJson());

class Resp {
  Resp({
    this.displayLink,
    this.id,
    this.imagePath,
    this.keyword,
    this.pdfCreatedAt,
    this.pdfId,
    this.pdfLink,
    this.pdfPageNo,
    this.save,
    this.tableName,
    this.title,
  });

  Resp.fromJson(dynamic json) {
    displayLink = json['displayLink'];
    id = json['id'];
    imagePath = json['image_path'];
    keyword = json['keyword'];
    pdfCreatedAt = json['pdf_created_at'];
    pdfId = json['pdf_id'];
    pdfLink = json['pdf_link'];
    pdfPageNo = json['pdf_page_no'];
    save = json['save'];
    tableName = json['table_name'];
    title = json['title'];
  }

  String? displayLink;
  int? id;
  String? imagePath;
  String? keyword;
  String? pdfCreatedAt;
  int? pdfId;
  String? pdfLink;
  int? pdfPageNo;
  int? save;
  String? tableName;
  String? title;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['displayLink'] = displayLink;
    map['id'] = id;
    map['image_path'] = imagePath;
    map['keyword'] = keyword;
    map['pdf_created_at'] = pdfCreatedAt;
    map['pdf_id'] = pdfId;
    map['pdf_link'] = pdfLink;
    map['pdf_page_no'] = pdfPageNo;
    map['save'] = save;
    map['table_name'] = tableName;
    map['title'] = title;
    return map;
  }
}
