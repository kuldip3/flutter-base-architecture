import 'dart:convert';

ProfileDataResponseModel profileDataResponseModelFromJson(String str) =>
    ProfileDataResponseModel.fromJson(json.decode(str));

String profileDataResponseModelToJson(ProfileDataResponseModel data) =>
    json.encode(data.toJson());

class ProfileDataResponseModel {
  ProfileDataResponseModel({
    this.data,
    this.message,
    this.success,
  });

  ProfileDataResponseModel.fromJson(dynamic json) {
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
    message = json['message'];
    success = json['success'];
  }

  Data? data;
  String? message;
  bool? success;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (data != null) {
      map['data'] = data?.toJson();
    }
    map['message'] = message;
    map['success'] = success;
    return map;
  }
}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));

String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.profileData,
  });

  Data.fromJson(dynamic json) {
    profileData =
        json['data'] != null ? ProfileData.fromJson(json['data']) : null;
  }

  ProfileData? profileData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (profileData != null) {
      map['data'] = profileData?.toJson();
    }
    return map;
  }
}

ProfileData companyDataFromJson(String str) =>
    ProfileData.fromJson(json.decode(str));

String companyDataToJson(ProfileData data) => json.encode(data.toJson());

class ProfileData {
  ProfileData({
    this.companyInfo,
    this.createdAt,
    this.email,
    this.id,
    this.isActive,
    this.name,
    this.phone,
    this.pmsCompanyId,
    this.roleId,
    this.username,
  });

  ProfileData.fromJson(dynamic json) {
    companyInfo = json['companyInfo'] != null
        ? CompanyInfo.fromJson(json['companyInfo'])
        : null;
    createdAt = json['created_at'];
    email = json['email'];
    id = json['id'];
    isActive = json['is_active'];
    name = json['name'];
    phone = json['phone'];
    pmsCompanyId = json['pms_company_id'];
    roleId = json['role_id'];
    username = json['username'];
  }

  CompanyInfo? companyInfo;
  String? createdAt;
  String? email;
  int? id;
  bool? isActive;
  String? name;
  String? phone;
  int? pmsCompanyId;
  int? roleId;
  String? username;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (companyInfo != null) {
      map['companyInfo'] = companyInfo?.toJson();
    }
    map['created_at'] = createdAt;
    map['email'] = email;
    map['id'] = id;
    map['is_active'] = isActive;
    map['name'] = name;
    map['phone'] = phone;
    map['pms_company_id'] = pmsCompanyId;
    map['role_id'] = roleId;
    map['username'] = username;
    return map;
  }
}

CompanyInfo companyInfoFromJson(String str) =>
    CompanyInfo.fromJson(json.decode(str));

String companyInfoToJson(CompanyInfo data) => json.encode(data.toJson());

class CompanyInfo {
  CompanyInfo({
    this.companyAddress,
    this.companyContactPerson,
    this.companyEmail,
    this.companyFax,
    this.companyName,
    this.companyPhone,
    this.companyRegistrationNo,
    this.designation,
  });

  CompanyInfo.fromJson(dynamic json) {
    companyAddress = json['company_address'];
    companyContactPerson = json['company_contact_person'];
    companyEmail = json['company_email'];
    companyFax = json['company_fax'];
    companyName = json['company_name'];
    companyPhone = json['company_phone'];
    companyRegistrationNo = json['company_registration_no'];
    designation = json['designation'];
  }

  dynamic companyAddress;
  dynamic companyContactPerson;
  dynamic companyEmail;
  dynamic companyFax;
  String? companyName;
  dynamic companyPhone;
  dynamic companyRegistrationNo;
  String? designation;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['company_address'] = companyAddress;
    map['company_contact_person'] = companyContactPerson;
    map['company_email'] = companyEmail;
    map['company_fax'] = companyFax;
    map['company_name'] = companyName;
    map['company_phone'] = companyPhone;
    map['company_registration_no'] = companyRegistrationNo;
    map['designation'] = designation;
    return map;
  }
}
