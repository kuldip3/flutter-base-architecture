import 'dart:convert';

/// access_token : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjo0LCJpYXQiOjE2NDQ0NzY0NzEsImV4cCI6MTY0NzA2ODQ3MX0.yODrLgr6v2Ck_2YyMwZnKqK5sQJfxsLfwZHjy8rO00Q"
/// refresh_token : ""
/// expiry : "30d"
/// message : "Login successful!"

LoginResponseModel loginResponseModelFromJson(String str) =>
    LoginResponseModel.fromJson(json.decode(str));

String loginResponseModelToJson(LoginResponseModel data) =>
    json.encode(data.toJson());

class LoginResponseModel {
  LoginResponseModel({
    String? accessToken,
    String? refreshToken,
    String? expiry,
    String? message,
  }) {
    _accessToken = accessToken;
    _refreshToken = refreshToken;
    _expiry = expiry;
    _message = message;
  }

  LoginResponseModel.fromJson(dynamic json) {
    _accessToken = json['access_token'];
    _refreshToken = json['refresh_token'];
    _expiry = json['expiry'];
    _message = json['message'];
  }

  String? _accessToken;
  String? _refreshToken;
  String? _expiry;
  String? _message;

  LoginResponseModel copyWith({
    String? accessToken,
    String? refreshToken,
    String? expiry,
    String? message,
  }) =>
      LoginResponseModel(
        accessToken: accessToken ?? _accessToken,
        refreshToken: refreshToken ?? _refreshToken,
        expiry: expiry ?? _expiry,
        message: message ?? _message,
      );

  String? get accessToken => _accessToken;

  String? get refreshToken => _refreshToken;

  String? get expiry => _expiry;

  String? get message => _message;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['access_token'] = _accessToken;
    map['refresh_token'] = _refreshToken;
    map['expiry'] = _expiry;
    map['message'] = _message;
    return map;
  }
}
