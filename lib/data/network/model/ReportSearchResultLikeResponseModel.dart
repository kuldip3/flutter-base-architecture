import 'dart:convert';

ReportSearchResultLikeResponseModel reportSearchResultLikeResponseModelFromJson(
        String str) =>
    ReportSearchResultLikeResponseModel.fromJson(json.decode(str));

String reportSearchResultLikeResponseModelToJson(
        ReportSearchResultLikeResponseModel data) =>
    json.encode(data.toJson());

class ReportSearchResultLikeResponseModel {
  ReportSearchResultLikeResponseModel({
    Data? data,
    String? message,
    bool? success,
  }) {
    _data = data;
    _message = message;
    _success = success;
  }

  ReportSearchResultLikeResponseModel.fromJson(dynamic json) {
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _message = json['message'];
    _success = json['success'];
  }

  Data? _data;
  String? _message;
  bool? _success;

  Data? get data => _data;

  String? get message => _message;

  bool? get success => _success;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['message'] = _message;
    map['success'] = _success;
    return map;
  }
}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));

String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    int? dislikesCount,
    int? id,
    int? itemId,
    dynamic jsonObject,
    int? like,
    int? likesCount,
    int? userId,
  }) {
    _dislikesCount = dislikesCount;
    _id = id;
    _itemId = itemId;
    _jsonObject = jsonObject;
    _like = like;
    _likesCount = likesCount;
    _userId = userId;
  }

  Data.fromJson(dynamic json) {
    _dislikesCount = json['dislikes_count'];
    _id = json['id'];
    _itemId = json['item_id'];
    _jsonObject = json['json_object'];
    _like = json['like'];
    _likesCount = json['likes_count'];
    _userId = json['user_id'];
  }

  int? _dislikesCount;
  int? _id;
  int? _itemId;
  dynamic _jsonObject;
  int? _like;
  int? _likesCount;
  int? _userId;

  int? get dislikesCount => _dislikesCount;

  int? get id => _id;

  int? get itemId => _itemId;

  dynamic get jsonObject => _jsonObject;

  int? get like => _like;

  int? get likesCount => _likesCount;

  int? get userId => _userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['dislikes_count'] = _dislikesCount;
    map['id'] = _id;
    map['item_id'] = _itemId;
    map['json_object'] = _jsonObject;
    map['like'] = _like;
    map['likes_count'] = _likesCount;
    map['user_id'] = _userId;
    return map;
  }
}
