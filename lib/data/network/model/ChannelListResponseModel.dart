import 'dart:convert';

ChannelListResponseModel channelListResponseModelFromJson(String str) =>
    ChannelListResponseModel.fromJson(json.decode(str));

String channelListResponseModelToJson(ChannelListResponseModel data) =>
    json.encode(data.toJson());

class ChannelListResponseModel {
  ChannelListResponseModel({
    this.data,
    this.message,
    this.success,
  });

  ChannelListResponseModel.fromJson(dynamic json) {
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
    message = json['message'];
    success = json['success'];
  }

  Data? data;
  String? message;
  bool? success;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (data != null) {
      map['data'] = data?.toJson();
    }
    map['message'] = message;
    map['success'] = success;
    return map;
  }
}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));

String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.channelsList,
  });

  Data.fromJson(dynamic json) {
    if (json['channels_list'] != null) {
      channelsList = [];
      json['channels_list'].forEach((v) {
        channelsList?.add(ChannelsList.fromJson(v));
      });
    }
  }

  List<ChannelsList>? channelsList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (channelsList != null) {
      map['channels_list'] = channelsList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

ChannelsList channelsListFromJson(String str) =>
    ChannelsList.fromJson(json.decode(str));

String channelsListToJson(ChannelsList data) => json.encode(data.toJson());

class ChannelsList {
  ChannelsList({
    this.channelSlug,
    this.extra,
    this.id,
    this.name,
  });

  ChannelsList.fromJson(dynamic json) {
    channelSlug = json['channel_slug'];
    extra = json['extra'];
    id = json['id'];
    name = json['name'];
  }

  String? channelSlug;
  dynamic extra;
  int? id;
  String? name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['channel_slug'] = channelSlug;
    map['extra'] = extra;
    map['id'] = id;
    map['name'] = name;
    return map;
  }
}
