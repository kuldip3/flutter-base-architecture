import 'dart:convert';

ReportSearchResponseModel reportSearchResponseModelFromJson(String str) =>
    ReportSearchResponseModel.fromJson(json.decode(str));

String reportSearchResponseModelToJson(ReportSearchResponseModel data) =>
    json.encode(data.toJson());

class ReportSearchResponseModel {
  ReportSearchResponseModel({
    this.data,
    this.message,
    this.success,
  });

  ReportSearchResponseModel.fromJson(dynamic json) {
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
    message = json['message'];
    success = json['success'];
  }

  Data? data;
  String? message;
  bool? success;

  ReportSearchResponseModel copyWith({
    Data? data,
    String? message,
    bool? success,
  }) =>
      ReportSearchResponseModel(
        data: data ?? this.data,
        message: message ?? this.message,
        success: success ?? this.success,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (data != null) {
      map['data'] = data?.toJson();
    }
    map['message'] = message;
    map['success'] = success;
    return map;
  }
}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));

String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    this.resp,
    this.resp1,
  });

  Data.fromJson(dynamic json) {
    if (json['resp'] != null) {
      resp = [];
      json['resp'].forEach((v) {
        resp?.add(Resp.fromJson(v));
      });
    }
    if (json['resp1'] != null) {
      resp1 = [];
      json['resp1'].forEach((v) {
        resp1?.add(Resp1.fromJson(v));
      });
    }
  }

  List<Resp>? resp;
  List<Resp1>? resp1;

  Data copyWith({
    List<Resp>? resp,
    List<Resp1>? resp1,
  }) =>
      Data(
        resp: resp ?? this.resp,
        resp1: resp1 ?? this.resp1,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (resp != null) {
      map['resp'] = resp?.map((v) => v.toJson()).toList();
    }
    if (resp1 != null) {
      map['resp1'] = resp1?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

Resp1 resp1FromJson(String str) => Resp1.fromJson(json.decode(str));

String resp1ToJson(Resp1 data) => json.encode(data.toJson());

class Resp1 {
  Resp1({
    this.dislike,
    this.displayLink,
    this.id,
    this.indiaRelatedWordCount,
    this.isUseful,
    this.keyword,
    this.keywordCount,
    this.like,
    this.pdfCreatedAt,
    this.pdfName,
    this.resourceLink,
    this.richness,
    this.save,
    this.searchDate,
    this.snippet,
    this.thumbnail,
    this.title,
    this.totalDislikes,
    this.totalLikes,
  });

  Resp1.fromJson(dynamic json) {
    dislike = json['dislike'];
    displayLink = json['displayLink'];
    id = json['id'];
    indiaRelatedWordCount = json['india_related_word_count'];
    isUseful = json['is_useful'];
    keyword = json['keyword'];
    keywordCount = json['keyword_count'];
    like = json['like'];
    pdfCreatedAt = json['pdf_created_at'];
    pdfName = json['pdf_name'];
    resourceLink = json['resource_link'];
    richness = json['richness'];
    save = json['save'];
    searchDate = json['search_date'];
    snippet = json['snippet'];
    thumbnail = json['thumbnail'];
    title = json['title'];
    totalDislikes = json['total_dislikes'];
    totalLikes = json['total_likes'];
  }

  int? dislike;
  String? displayLink;
  int? id;
  int? indiaRelatedWordCount;
  int? isUseful;
  String? keyword;
  int? keywordCount;
  int? like;
  String? pdfCreatedAt;
  String? pdfName;
  String? resourceLink;
  int? richness;
  int? save;
  String? searchDate;
  String? snippet;
  String? thumbnail;
  String? title;
  int? totalDislikes;
  int? totalLikes;

  Resp1 copyWith({
    int? dislike,
    String? displayLink,
    int? id,
    int? indiaRelatedWordCount,
    int? isUseful,
    String? keyword,
    int? keywordCount,
    int? like,
    String? pdfCreatedAt,
    String? pdfName,
    String? resourceLink,
    int? richness,
    int? save,
    String? searchDate,
    String? snippet,
    String? thumbnail,
    String? title,
    int? totalDislikes,
    int? totalLikes,
  }) =>
      Resp1(
        dislike: dislike ?? this.dislike,
        displayLink: displayLink ?? this.displayLink,
        id: id ?? this.id,
        indiaRelatedWordCount:
            indiaRelatedWordCount ?? this.indiaRelatedWordCount,
        isUseful: isUseful ?? this.isUseful,
        keyword: keyword ?? this.keyword,
        keywordCount: keywordCount ?? this.keywordCount,
        like: like ?? this.like,
        pdfCreatedAt: pdfCreatedAt ?? this.pdfCreatedAt,
        pdfName: pdfName ?? this.pdfName,
        resourceLink: resourceLink ?? this.resourceLink,
        richness: richness ?? this.richness,
        save: save ?? this.save,
        searchDate: searchDate ?? this.searchDate,
        snippet: snippet ?? this.snippet,
        thumbnail: thumbnail ?? this.thumbnail,
        title: title ?? this.title,
        totalDislikes: totalDislikes ?? this.totalDislikes,
        totalLikes: totalLikes ?? this.totalLikes,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['dislike'] = dislike;
    map['displayLink'] = displayLink;
    map['id'] = id;
    map['india_related_word_count'] = indiaRelatedWordCount;
    map['is_useful'] = isUseful;
    map['keyword'] = keyword;
    map['keyword_count'] = keywordCount;
    map['like'] = like;
    map['pdf_created_at'] = pdfCreatedAt;
    map['pdf_name'] = pdfName;
    map['resource_link'] = resourceLink;
    map['richness'] = richness;
    map['save'] = save;
    map['search_date'] = searchDate;
    map['snippet'] = snippet;
    map['thumbnail'] = thumbnail;
    map['title'] = title;
    map['total_dislikes'] = totalDislikes;
    map['total_likes'] = totalLikes;
    return map;
  }
}

Resp respFromJson(String str) => Resp.fromJson(json.decode(str));

String respToJson(Resp data) => json.encode(data.toJson());

class Resp {
  Resp({
    this.dislike,
    this.displayLink,
    this.id,
    this.indiaRelatedWordCount,
    this.isUseful,
    this.keyword,
    this.keywordCount,
    this.like,
    this.pdfCreatedAt,
    this.pdfName,
    this.resourceLink,
    this.richness,
    this.save,
    this.searchDate,
    this.snippet,
    this.thumbnail,
    this.title,
    this.totalDislikes,
    this.totalLikes,
  });

  Resp.fromJson(dynamic json) {
    dislike = json['dislike'];
    displayLink = json['displayLink'];
    id = json['id'];
    indiaRelatedWordCount = json['india_related_word_count'];
    isUseful = json['is_useful'];
    keyword = json['keyword'];
    keywordCount = json['keyword_count'];
    like = json['like'];
    pdfCreatedAt = json['pdf_created_at'];
    pdfName = json['pdf_name'];
    resourceLink = json['resource_link'];
    richness = json['richness'];
    save = json['save'];
    searchDate = json['search_date'];
    snippet = json['snippet'];
    thumbnail = json['thumbnail'];
    title = json['title'];
    totalDislikes = json['total_dislikes'];
    totalLikes = json['total_likes'];
  }

  int? dislike;
  String? displayLink;
  int? id;
  int? indiaRelatedWordCount;
  int? isUseful;
  String? keyword;
  int? keywordCount;
  int? like;
  String? pdfCreatedAt;
  String? pdfName;
  String? resourceLink;
  int? richness;
  int? save;
  String? searchDate;
  String? snippet;
  String? thumbnail;
  String? title;
  int? totalDislikes;
  int? totalLikes;

  Resp copyWith({
    int? dislike,
    String? displayLink,
    int? id,
    int? indiaRelatedWordCount,
    int? isUseful,
    String? keyword,
    int? keywordCount,
    int? like,
    String? pdfCreatedAt,
    String? pdfName,
    String? resourceLink,
    int? richness,
    int? save,
    String? searchDate,
    String? snippet,
    String? thumbnail,
    String? title,
    int? totalDislikes,
    int? totalLikes,
  }) =>
      Resp(
        dislike: dislike ?? this.dislike,
        displayLink: displayLink ?? this.displayLink,
        id: id ?? this.id,
        indiaRelatedWordCount:
            indiaRelatedWordCount ?? this.indiaRelatedWordCount,
        isUseful: isUseful ?? this.isUseful,
        keyword: keyword ?? this.keyword,
        keywordCount: keywordCount ?? this.keywordCount,
        like: like ?? this.like,
        pdfCreatedAt: pdfCreatedAt ?? this.pdfCreatedAt,
        pdfName: pdfName ?? this.pdfName,
        resourceLink: resourceLink ?? this.resourceLink,
        richness: richness ?? this.richness,
        save: save ?? this.save,
        searchDate: searchDate ?? this.searchDate,
        snippet: snippet ?? this.snippet,
        thumbnail: thumbnail ?? this.thumbnail,
        title: title ?? this.title,
        totalDislikes: totalDislikes ?? this.totalDislikes,
        totalLikes: totalLikes ?? this.totalLikes,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['dislike'] = dislike;
    map['displayLink'] = displayLink;
    map['id'] = id;
    map['india_related_word_count'] = indiaRelatedWordCount;
    map['is_useful'] = isUseful;
    map['keyword'] = keyword;
    map['keyword_count'] = keywordCount;
    map['like'] = like;
    map['pdf_created_at'] = pdfCreatedAt;
    map['pdf_name'] = pdfName;
    map['resource_link'] = resourceLink;
    map['richness'] = richness;
    map['save'] = save;
    map['search_date'] = searchDate;
    map['snippet'] = snippet;
    map['thumbnail'] = thumbnail;
    map['title'] = title;
    map['total_dislikes'] = totalDislikes;
    map['total_likes'] = totalLikes;
    return map;
  }
}
