import "package:dio/dio.dart";
import 'package:get/get.dart';
import 'package:zebrapro_announcements/config/ApiConfig.dart';
import 'package:zebrapro_announcements/data/AppDataManager.dart';
import 'package:zebrapro_announcements/data/network/pretty_dio_logger.dart';

class Client {
  String APP_BASEURL = ApiConfig.APP_BASE_URL;
  String WS_BASEURL = ApiConfig.WS_BASE_URL;
  String APIKEY = "";
  Dio? _dio;
  BaseOptions options = BaseOptions(
    connectTimeout: 10000,
    receiveTimeout: 5000,
  );

  // String? token;
  Map<String, Object>? header;

  // Client({this.token});

  Client builder() {
    header = <String, Object>{};
    header!.putIfAbsent('Accept', () => 'application/json');
    // header!.putIfAbsent('x-api-key', () => APIKEY);
    header!.putIfAbsent('Content-Type', () => 'application/json');
    _dio = Dio(options);
    _dio!.interceptors.add(hy);
    _dio!.interceptors.add(PrettyDioLogger());
    _dio!.options.headers = header;
    return this;
  }

  Client acceptHtml() {
    header!.remove('Accept');
    return this;
  }

  Client workstationUrl() {
    _dio!.options.baseUrl = WS_BASEURL;
    return this;
  }

  Client appUrl() {
    _dio!.options.baseUrl = APP_BASEURL;
    return this;
  }

  Client setUrlEncoded() {
    header!.remove('Content-Type');
    header!
        .putIfAbsent('Content-Type', () => 'application/x-www-form-urlencoded');
    _dio!.options.headers = header;
    return this;
  }

  Client removeContentType() {
    header!.remove('Content-Type');
    return this;
  }

  Client removeAndAddAccept() {
    header!.remove('Accept');
    header!.putIfAbsent("Accept", () => "*/*");
    return this;
  }

  Client setMultipartFormDataHeader() {
    header!.remove('Content-Type');
    header!.putIfAbsent('Content-Type', () => 'multipart/form-data');
    _dio!.options.headers = header;
    return this;
  }

  Client setProtectedApiHeader() {
    AppDataManager _dataManager = Get.find<AppDataManager>();
    String? token = _dataManager.getServerAuthToken();
    //get token from shared preference
    header!.putIfAbsent('Authorization', () => 'Bearer $token');
    return this;
  }

  Dio build() {
    return _dio!;
  }
}

InterceptorsWrapper hy = InterceptorsWrapper(
  onRequest: (options, handler) {
    return handler.next(options);
  },
  onResponse: (response, handler) {
    return handler.next(response);
  },
  onError: (DioError e, handler) {
    return handler.next(e);
  },
);
