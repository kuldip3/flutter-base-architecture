import 'package:zebrapro_announcements/data/network/model/AnnouncementCommentsResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/AnnouncementSaveCommentResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/AnnouncementsResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ApiResult/ApiResult.dart';
import 'package:zebrapro_announcements/data/network/model/BookmarkSaveResponse.dart';
import 'package:zebrapro_announcements/data/network/model/ChannelListResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ChannelSubscribeResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ChannelSubscriptionListResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/GeneralResponse.dart';
import 'package:zebrapro_announcements/data/network/model/IndustryResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/LoginRequestModel.dart';
import 'package:zebrapro_announcements/data/network/model/LoginResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ProfileDataResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ReportSearchProcessStatusResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ReportSearchResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ReportSearchResultLikeResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/SaveImpactResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/StatsScannerResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/VerifyTokenResponse.dart';
import 'package:zebrapro_announcements/data/network/model/requests/AnnouncementCommentRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/AnnouncementImpactRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/AnnouncementsRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/BookmarkRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/ChannelSubscribeRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/FirebaseTokenRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/NotificationStatusRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/ReportSearchProcessingStatusRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/ReportSearchRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/ReportSearchResultLikeRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/SendNotificationRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/StatsScannerRequest.dart';

abstract class ApiHelper {
  Future<ApiResult<LoginResponseModel>> loginApiCall(
      LoginRequestModel loginRequestModel);

  Future<ApiResult<bool>> logoutApiCall();

  Future<ApiResult<VerifyTokenResponse>> verifyTokenApiCall();

  Future<ApiResult<AnnouncementsResponseModel>> getAnnouncementsApiCall(
      AnnouncementsRequest request);

  //region Report Search
  Future<ApiResult<ReportSearchResponseModel>> getReportSearchApiCall(
      ReportSearchRequest request);

  Future<ApiResult<ReportSearchProcessStatusResponseModel>>
      getReportSearchProcessingStatusApiCall(
          ReportSearchProcessingStatusRequest request);

  Future<ApiResult<ReportSearchResultLikeResponseModel>>
      postLikeToReportSearchResultApiCall(
          ReportSearchResultLikeRequest request);

  //endregion
  Future<ApiResult<IndustryResponseModel>> getIndustryListApiCall();

  Future<ApiResult<StatsScannerResponseModel>> getStatsScannersApiCall(
      StatsScannerRequest request);

  //region Stats Scanner

  //endregion

  Future<ApiResult<BookmarkSaveResponse>> postBookmarkItemApiCall(
      BookmarkRequest request);

  Future<ApiResult<ProfileDataResponseModel>> getProfileData();

  Future<ApiResult<SaveImpactResponseModel>> deleteAnnouncementImpact(
      int impactId);

  Future<ApiResult<SaveImpactResponseModel>> postAnnouncementImpact(
      AnnouncementImpactRequest request);

  Future<ApiResult<AnnouncementCommentsResponseModel>>
      getAnnouncementCommentsApiCall(int postId);

  Future<ApiResult<AnnouncementSaveCommentResponseModel>>
      postCommentOnAnnouncementApiCall(AnnouncementCommentRequest request);

  Future<ApiResult<GeneralResponse>> postFirebaseTokenApiCall(
      FirebaseTokenRequest request);

  Future<ApiResult<ChannelListResponseModel>> getChannelListApiCall();

  Future<ApiResult<ChannelSubscribeResponseModel>>
      subscribeToNotificationChannel(ChannelSubscribeRequest request);

  Future<ApiResult<ChannelSubscriptionListResponseModel>>
      getChannelSubscriptionsApiCall();

  Future<ApiResult<GeneralResponse>> postSendNotification(
      SendNotificationRequest request);

  Future<ApiResult<GeneralResponse>> postNotificationStatus(
      NotificationStatusRequest request);
}
