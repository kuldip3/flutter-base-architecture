import 'package:get/get.dart';
import 'package:zebrapro_announcements/data/DataManager.dart';
import 'package:zebrapro_announcements/data/local/AppDbHelper.dart';
import 'package:zebrapro_announcements/data/network/AppApiHelper.dart';
import 'package:zebrapro_announcements/data/network/model/AnnouncementCommentsResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/AnnouncementSaveCommentResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/AnnouncementsResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ApiResult/ApiResult.dart';
import 'package:zebrapro_announcements/data/network/model/BookmarkSaveResponse.dart';
import 'package:zebrapro_announcements/data/network/model/ChannelListResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ChannelSubscribeResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ChannelSubscriptionListResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/GeneralResponse.dart';
import 'package:zebrapro_announcements/data/network/model/IndustryResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/LoginRequestModel.dart';
import 'package:zebrapro_announcements/data/network/model/LoginResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ProfileDataResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ReportSearchProcessStatusResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ReportSearchResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/ReportSearchResultLikeResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/SaveImpactResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/StatsScannerResponseModel.dart';
import 'package:zebrapro_announcements/data/network/model/VerifyTokenResponse.dart';
import 'package:zebrapro_announcements/data/network/model/requests/AnnouncementCommentRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/AnnouncementImpactRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/AnnouncementsRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/BookmarkRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/ChannelSubscribeRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/FirebaseTokenRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/NotificationStatusRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/ReportSearchProcessingStatusRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/ReportSearchRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/ReportSearchResultLikeRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/SendNotificationRequest.dart';
import 'package:zebrapro_announcements/data/network/model/requests/StatsScannerRequest.dart';
import 'package:zebrapro_announcements/data/prefs/AppPreferencesHelper.dart';

class AppDataManager implements DataManager {
  final AppPreferencesHelper _preferencesHelper =
      Get.find<AppPreferencesHelper>();
  final AppDbHelper _dbHelper = Get.find<AppDbHelper>();
  final AppApiHelper _apiHelper = Get.find<AppApiHelper>();

  @override
  String? getServerAuthToken() {
    return _preferencesHelper.getServerAuthToken();
  }

  @override
  void setServerAuthToken(String token) {
    _preferencesHelper.setServerAuthToken(token);
  }

  @override
  Future<ApiResult<LoginResponseModel>> loginApiCall(
      LoginRequestModel loginRequestModel) {
    return _apiHelper.loginApiCall(loginRequestModel);
  }

  @override
  Future<ApiResult<bool>> logoutApiCall() {
    return _apiHelper.logoutApiCall();
  }

  @override
  bool clearAllPreferences() {
    return _preferencesHelper.clearAllPreferences();
  }

  @override
  String? getCreatedAt() {
    return _preferencesHelper.getCreatedAt();
  }

  @override
  String? getEmail() {
    return _preferencesHelper.getEmail();
  }

  @override
  int? getId() {
    return _preferencesHelper.getId();
  }

  @override
  bool? getIsActive() {
    return _preferencesHelper.getIsActive();
  }

  @override
  String? getName() {
    return _preferencesHelper.getName();
  }

  @override
  String? getPhone() {
    return _preferencesHelper.getPhone();
  }

  @override
  int? getRoleId() {
    return _preferencesHelper.getRoleId();
  }

  @override
  String? getUsername() {
    return _preferencesHelper.getUsername();
  }

  @override
  void setCreatedAt(String createdAt) {
    _preferencesHelper.setCreatedAt(createdAt);
  }

  @override
  void setEmail(String email) {
    _preferencesHelper.setEmail(email);
  }

  @override
  void setId(int id) {
    _preferencesHelper.setId(id);
  }

  @override
  void setIsActive(bool isActive) {
    _preferencesHelper.setIsActive(isActive);
  }

  @override
  void setName(String name) {
    _preferencesHelper.setName(name);
  }

  @override
  void setPhone(String phone) {
    _preferencesHelper.setPhone(phone);
  }

  @override
  void setRoleId(int roleId) {
    _preferencesHelper.setRoleId(roleId);
  }

  @override
  void setUsername(String username) {
    _preferencesHelper.setUsername(username);
  }

  @override
  int? getPmsCompanyId() {
    return _preferencesHelper.getPmsCompanyId();
  }

  @override
  void setPmsCompanyId(int pmsCompanyId) {
    _preferencesHelper.setPmsCompanyId(pmsCompanyId);
  }

  @override
  Future<ApiResult<AnnouncementsResponseModel>> getAnnouncementsApiCall(
      AnnouncementsRequest request) {
    return _apiHelper.getAnnouncementsApiCall(request);
  }

  @override
  Future<ApiResult<ReportSearchResponseModel>> getReportSearchApiCall(
      ReportSearchRequest request) {
    return _apiHelper.getReportSearchApiCall(request);
  }

  @override
  Future<ApiResult<ReportSearchProcessStatusResponseModel>>
      getReportSearchProcessingStatusApiCall(
          ReportSearchProcessingStatusRequest request) {
    return _apiHelper.getReportSearchProcessingStatusApiCall(request);
  }

  @override
  Future<ApiResult<ReportSearchResultLikeResponseModel>>
      postLikeToReportSearchResultApiCall(
          ReportSearchResultLikeRequest request) {
    return _apiHelper.postLikeToReportSearchResultApiCall(request);
  }

  @override
  Future<ApiResult<VerifyTokenResponse>> verifyTokenApiCall() {
    return _apiHelper.verifyTokenApiCall();
  }

  @override
  Future<ApiResult<BookmarkSaveResponse>> postBookmarkItemApiCall(
      BookmarkRequest request) {
    return _apiHelper.postBookmarkItemApiCall(request);
  }

  @override
  Future<ApiResult<IndustryResponseModel>> getIndustryListApiCall() {
    return _apiHelper.getIndustryListApiCall();
  }

  @override
  Future<ApiResult<StatsScannerResponseModel>> getStatsScannersApiCall(
      StatsScannerRequest request) {
    return _apiHelper.getStatsScannersApiCall(request);
  }

  @override
  Future<ApiResult<ProfileDataResponseModel>> getProfileData() {
    return _apiHelper.getProfileData();
  }

  @override
  Future<ApiResult<SaveImpactResponseModel>> deleteAnnouncementImpact(
      int impactId) {
    return _apiHelper.deleteAnnouncementImpact(impactId);
  }

  @override
  Future<ApiResult<AnnouncementCommentsResponseModel>>
      getAnnouncementCommentsApiCall(int postId) {
    return _apiHelper.getAnnouncementCommentsApiCall(postId);
  }

  @override
  Future<ApiResult<SaveImpactResponseModel>> postAnnouncementImpact(
      AnnouncementImpactRequest request) {
    return _apiHelper.postAnnouncementImpact(request);
  }

  @override
  Future<ApiResult<AnnouncementSaveCommentResponseModel>>
      postCommentOnAnnouncementApiCall(AnnouncementCommentRequest request) {
    return _apiHelper.postCommentOnAnnouncementApiCall(request);
  }

  @override
  Future<ApiResult<ChannelListResponseModel>> getChannelListApiCall() {
    return _apiHelper.getChannelListApiCall();
  }

  @override
  Future<ApiResult<ChannelSubscriptionListResponseModel>>
      getChannelSubscriptionsApiCall() {
    return _apiHelper.getChannelSubscriptionsApiCall();
  }

  @override
  Future<ApiResult<GeneralResponse>> postFirebaseTokenApiCall(
      FirebaseTokenRequest request) {
    return _apiHelper.postFirebaseTokenApiCall(request);
  }

  @override
  Future<ApiResult<GeneralResponse>> postNotificationStatus(
      NotificationStatusRequest request) {
    return _apiHelper.postNotificationStatus(request);
  }

  @override
  Future<ApiResult<GeneralResponse>> postSendNotification(
      SendNotificationRequest request) {
    return _apiHelper.postSendNotification(request);
  }

  @override
  Future<ApiResult<ChannelSubscribeResponseModel>>
      subscribeToNotificationChannel(ChannelSubscribeRequest request) {
    return _apiHelper.subscribeToNotificationChannel(request);
  }
}
