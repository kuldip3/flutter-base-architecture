abstract class PreferencesHelper {
  String? getServerAuthToken();

  void setServerAuthToken(String token);

  int? getId();

  void setId(int id);

  int? getRoleId();

  void setRoleId(int roleId);

  String? getName();

  void setName(String name);

  String? getUsername();

  void setUsername(String username);

  String? getEmail();

  void setEmail(String email);

  String? getPhone();

  void setPhone(String phone);

  bool? getIsActive();

  void setIsActive(bool isActive);

  String? getCreatedAt();

  void setCreatedAt(String createdAt);

  int? getPmsCompanyId();

  void setPmsCompanyId(int pmsCompanyId);

  bool clearAllPreferences();
}
