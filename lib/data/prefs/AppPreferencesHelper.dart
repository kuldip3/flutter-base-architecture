import 'package:shared_preferences/shared_preferences.dart';
import 'package:zebrapro_announcements/data/prefs/PreferencesHelper.dart';

class AppPreferencesHelper implements PreferencesHelper {
  final SharedPreferences _prefs;
  static const PREF_ACCESS_TOKEN = "ACCESS_TOKEN";
  static const PREF_ROLE = "ROLE";
  static const PREF_SERVER_URL = "SERVER";

  static const PREF_ID = "ID";
  static const PREF_ROLE_ID = "ROLE_ID";
  static const PREF_NAME = "NAME";
  static const PREF_USERNAME = "USERNAME";
  static const PREF_EMAIL = "EMAIL";
  static const PREF_PHONE = "PHONE";
  static const PREF_IS_ACTIVE = "IS_ACTIVE";
  static const PREF_CREATED_AT = "CREATED_AT";
  static const PREF_PMS_COMPANY_ID = "PMS_COMPANY_ID";

  AppPreferencesHelper(this._prefs);

  @override
  String? getServerAuthToken() {
    try {
      return _prefs.getString(PREF_ACCESS_TOKEN);
    } catch (e) {
      return null;
    }
  }

  @override
  void setServerAuthToken(String token) {
    try {
      _prefs.setString(PREF_ACCESS_TOKEN, token);
    } catch (e) {
      return;
    }
  }

  @override
  bool clearAllPreferences() {
    try {
      _prefs.clear();
      return true;
    } catch (e) {
      return false;
    }
  }

  @override
  String? getCreatedAt() {
    try {
      return _prefs.getString(PREF_CREATED_AT);
    } catch (e) {
      return null;
    }
  }

  @override
  String? getEmail() {
    try {
      return _prefs.getString(PREF_EMAIL);
    } catch (e) {
      return null;
    }
  }

  @override
  int? getId() {
    try {
      return _prefs.getInt(PREF_ID);
    } catch (e) {
      return null;
    }
  }

  @override
  bool? getIsActive() {
    try {
      return _prefs.getBool(PREF_IS_ACTIVE);
    } catch (e) {
      return null;
    }
  }

  @override
  String? getName() {
    try {
      return _prefs.getString(PREF_NAME);
    } catch (e) {
      return null;
    }
  }

  @override
  String? getPhone() {
    try {
      return _prefs.getString(PREF_PHONE);
    } catch (e) {
      return null;
    }
  }

  @override
  int? getRoleId() {
    try {
      return _prefs.getInt(PREF_ROLE_ID);
    } catch (e) {
      return null;
    }
  }

  @override
  String? getUsername() {
    try {
      return _prefs.getString(PREF_USERNAME);
    } catch (e) {
      return null;
    }
  }

  @override
  void setCreatedAt(String createdAt) {
    try {
      _prefs.setString(PREF_CREATED_AT, createdAt);
    } catch (e) {
      return;
    }
  }

  @override
  void setEmail(String email) {
    try {
      _prefs.setString(PREF_EMAIL, email);
    } catch (e) {
      return;
    }
  }

  @override
  void setId(int id) {
    try {
      _prefs.setInt(PREF_ID, id);
    } catch (e) {
      return;
    }
  }

  @override
  void setIsActive(bool isActive) {
    try {
      _prefs.setBool(PREF_IS_ACTIVE, isActive);
    } catch (e) {
      return;
    }
  }

  @override
  void setName(String name) {
    try {
      _prefs.setString(PREF_NAME, name);
    } catch (e) {
      return;
    }
  }

  @override
  void setPhone(String phone) {
    try {
      _prefs.setString(PREF_PHONE, phone);
    } catch (e) {
      return;
    }
  }

  @override
  void setRoleId(int roleId) {
    try {
      _prefs.setInt(PREF_ROLE_ID, roleId);
    } catch (e) {
      return;
    }
  }

  @override
  void setUsername(String username) {
    try {
      _prefs.setString(PREF_USERNAME, username);
    } catch (e) {
      return;
    }
  }

  @override
  int? getPmsCompanyId() {
    return _prefs.getInt(PREF_PMS_COMPANY_ID);
  }

  @override
  void setPmsCompanyId(int pmsCompanyId) {
    try {
      _prefs.setInt(PREF_PMS_COMPANY_ID, pmsCompanyId);
    } catch (e) {
      return;
    }
  }
}
