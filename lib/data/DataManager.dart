import 'package:zebrapro_announcements/data/local/DbHelper.dart';
import 'package:zebrapro_announcements/data/network/ApiHelper.dart';
import 'package:zebrapro_announcements/data/prefs/PreferencesHelper.dart';

abstract class DataManager implements ApiHelper, DbHelper, PreferencesHelper {}
