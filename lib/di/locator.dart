import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:zebrapro_announcements/data/AppDataManager.dart';
import 'package:zebrapro_announcements/data/local/AppDbHelper.dart';
import 'package:zebrapro_announcements/data/network/AppApiHelper.dart';
import 'package:zebrapro_announcements/data/network/Client.dart';
import 'package:zebrapro_announcements/data/prefs/AppPreferencesHelper.dart';
import 'package:zebrapro_announcements/ui/base/BaseViewModel.dart';
import 'package:zebrapro_announcements/ui/screens/announcements/AnnouncemetsViewModel.dart';
import 'package:zebrapro_announcements/ui/screens/announcements/announcement_card/AnnouncementCardViewModel.dart';
import 'package:zebrapro_announcements/ui/screens/home/HomeViewModel.dart';
import 'package:zebrapro_announcements/ui/screens/login/LoginViewModel.dart';
import 'package:zebrapro_announcements/ui/screens/profile/ProfileViewModel.dart';
import 'package:zebrapro_announcements/ui/screens/report_search/ReportSearchViewModel.dart';
import 'package:zebrapro_announcements/ui/screens/report_search/report_results/ReportSearchViewModel.dart';
import 'package:zebrapro_announcements/ui/screens/settings/SettingsViewModel.dart';
import 'package:zebrapro_announcements/ui/screens/startup/StartupViewModel.dart';
import 'package:zebrapro_announcements/ui/screens/states_scanner/StatesScannerViewModel.dart';
import 'package:zebrapro_announcements/ui/screens/states_scanner/states_scanner_result/StatesScannerResultViewModel.dart';
import 'package:zebrapro_announcements/ui/widgets/nav_drawer/NavDrawerViewModel.dart';

Future setupLocator() async {
  Get.put<Client>(Client(), permanent: true);
  Get.put<AppApiHelper>(AppApiHelper(), permanent: true);
  Get.put<AppDbHelper>(AppDbHelper(), permanent: true);
  await Get.putAsync<AppPreferencesHelper>(
      () async => AppPreferencesHelper(await SharedPreferences.getInstance()),
      permanent: true);
  Get.put<AppDataManager>(AppDataManager(), permanent: true);
  Get.put<BottomSheetService>(BottomSheetService(), permanent: true);
  Get.put<DialogService>(DialogService(), permanent: true);

  Get.put<BaseViewModel>(BaseViewModel(), permanent: true);

  Get.put<StartupViewModel>(StartupViewModel(), permanent: true);
  Get.put<LoginViewModel>(LoginViewModel(), permanent: true);
  Get.put<HomeViewModel>(HomeViewModel(), permanent: true);
  Get.put<NavDrawerViewModel>(NavDrawerViewModel(), permanent: true);
  Get.put<AnnouncementViewModel>(AnnouncementViewModel(), permanent: true);
  Get.put<ReportSearchViewModel>(ReportSearchViewModel(), permanent: true);
  Get.put<ReportSearchResultViewModel>(ReportSearchResultViewModel(),
      permanent: true);
  Get.put<StatesScannerViewModel>(StatesScannerViewModel(), permanent: true);
  Get.put<StatesScannerResultViewModel>(StatesScannerResultViewModel(),
      permanent: true);
  Get.put<ProfileViewModel>(ProfileViewModel(), permanent: true);
  Get.put<AnnouncementCardViewModel>(AnnouncementCardViewModel(),
      permanent: true);
  Get.put<SettingsViewModel>(SettingsViewModel(), permanent: true);
}
