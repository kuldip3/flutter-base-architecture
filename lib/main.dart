import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:zebrapro_announcements/config/ColorConfig.dart';
import 'package:zebrapro_announcements/di/locator.dart';
import 'package:zebrapro_announcements/routes/Routes.dart';
import 'package:zebrapro_announcements/services/AppFirebaseMessagingServices.dart';
import 'package:zebrapro_announcements/ui/screens/startup/StartupView.dart';
import 'package:zebrapro_announcements/utils/setupBottomSheetUi.dart';
import 'package:zebrapro_announcements/utils/SetupDialogUi.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

//flutter pub run build_runner build --delete-conflicting-outputs
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseAnalytics analytics = FirebaseAnalytics.instance;

  FirebaseMessaging.onBackgroundMessage(
      AppFirebaseMessagingServices().firebaseMessagingBackgroundHandler);

  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) async {
    await dotenv.load(fileName: ".env");
    try {
      await setupLocator();
    } catch (e) {
      print(e);
    }
    setUpBototmSheet();
    setupDialogUi();

    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    EasyLoading.instance
      ..indicatorType = EasyLoadingIndicatorType.circle
      ..loadingStyle = EasyLoadingStyle.custom
      ..maskColor = Colors.black.withOpacity(0.2)
      ..maskType = EasyLoadingMaskType.custom
      ..backgroundColor = ColorConfig.accentColor.withAlpha(1)
      ..textColor = Colors.white
      ..indicatorColor = Colors.white;

    return GetMaterialApp(
        builder: EasyLoading.init(),
        title: 'ZebraPro',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: 'Montserrat',
          colorScheme: ColorScheme.fromSwatch().copyWith(
            secondary: ColorConfig.accentColor,
            primary: ColorConfig.primaryColor,
          ),
          scaffoldBackgroundColor: Colors.white,
          textSelectionTheme: TextSelectionThemeData(
            cursorColor: ColorConfig.secondaryTextColor,
            selectionColor: ColorConfig.lightAccentColor,
            selectionHandleColor: ColorConfig.accentColor,
          ),
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
                primary: ColorConfig.accentColor,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12)))),
          ),
        ),
        home: const StartupView(),
        getPages: RouteClass.routes,
        localizationsDelegates: [FormBuilderLocalizations.delegate]);
  }
}
