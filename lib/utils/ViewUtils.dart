import 'package:flutter/material.dart';

class ViewUtils {
  static double getHeightPercent(context, int heightInPercent) {
    return MediaQuery.of(context).size.height * (heightInPercent / 100);
  }

  static double getWidthPercent(context, int widthInPercent) {
    return MediaQuery.of(context).size.width * (widthInPercent / 100);
  }
}
