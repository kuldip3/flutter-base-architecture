import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:zebrapro_announcements/config/ColorConfig.dart';

import 'enums/DialogEnum.dart';

void setupDialogUi() {
  final dialogService = Get.find<DialogService>();

  final builders = {
    DialogEnum.saveOrNot: (context, sheetRequest, completer) =>
        LogoutDialog(request: sheetRequest, completer: completer),
    DialogEnum.success: (context, sheetRequest, completer) =>
        SuccessDialog(request: sheetRequest, completer: completer),
    DialogEnum.confirmation: (context, sheetRequest, completer) =>
        ConfirmationDialog(request: sheetRequest, completer: completer),
    DialogEnum.failure: (context, sheetRequest, completer) =>
        FailureDialog(request: sheetRequest, completer: completer),
    DialogEnum.customPlan: (context, sheetRequest, completer) =>
        CustomPlanDialog(request: sheetRequest, completer: completer),
  };

  dialogService.registerCustomDialogBuilders(builders);
}

class FailureDialog extends StatelessWidget {
  final DialogRequest request;
  final Function(DialogResponse) completer;

  const FailureDialog({required this.request, required this.completer});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(30.0),
      child: Center(
        child: Stack(
          alignment: AlignmentDirectional.topCenter,
          children: [
            Container(
              color: Colors.white,
              child: Padding(
                padding:
                    EdgeInsets.only(left: 13, top: 50, right: 13, bottom: 15),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        request.title ?? "Failure",
                        style: TextStyle(fontSize: 23),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Text(request.description ?? "Some problem occured",
                          style: TextStyle(fontSize: 16, color: Colors.grey)),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 10, right: 10, top: 20, bottom: 10),
                        child: GestureDetector(
                          onTap: () {
                            completer(DialogResponse(confirmed: true));
                          },
                          child: Container(
                            color: ColorConfig.red,
                            child: Padding(
                              padding: const EdgeInsets.all(14.0),
                              child: Center(
                                  child: Text(
                                "OK",
                                style: TextStyle(color: Colors.white),
                              )),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              top: -50,
              child: Container(
                width: 100,
                height: 100, //
                child: Image.asset("assets/images/wrong.png"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SuccessDialog extends StatelessWidget {
  final DialogRequest request;
  final Function(DialogResponse) completer;

  const SuccessDialog({required this.request, required this.completer});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(30.0),
      child: Center(
        child: Stack(
          alignment: AlignmentDirectional.topCenter,
          children: [
            Container(
              color: Colors.white,
              child: Padding(
                padding:
                    EdgeInsets.only(left: 13, top: 50, right: 13, bottom: 15),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        request.title ?? "Success",
                        style: TextStyle(fontSize: 23),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Text(
                          request.description ??
                              "Information saved Succesfully",
                          style: TextStyle(fontSize: 16, color: Colors.grey)),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 10, right: 10, top: 20, bottom: 10),
                        child: GestureDetector(
                          onTap: () {
                            completer(DialogResponse(confirmed: true));
                          },
                          child: Container(
                            color: ColorConfig.green,
                            child: Padding(
                              padding: const EdgeInsets.all(14.0),
                              child: Center(
                                  child: Text(
                                request.mainButtonTitle ?? "OK",
                                style: TextStyle(color: Colors.white),
                              )),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              top: -50,
              child: Container(
                width: 100,
                height: 100, //
                child: Image.asset("assets/images/right_success.png"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class LogoutDialog extends StatelessWidget {
  final DialogRequest request;
  final Function(DialogResponse) completer;

  LogoutDialog({required this.request, required this.completer});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        child: Padding(
          padding: EdgeInsets.only(left: 13, top: 13, right: 21, bottom: 15),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 10.0, top: 15.0, bottom: 15.0),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.power_settings_new, color: ColorConfig.grey),
                      const SizedBox(
                        width: 8,
                      ),
                      Text(
                        request.title ?? "Are you sure",
                        style: TextStyle(
                          fontSize: 21,
                          color: ColorConfig.grey,
                          fontFamily: 'ProximaNova',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text(
                    request.description ?? "Are you sure you want to logout?",
                    style: const TextStyle(
                        fontSize: 16,
                        color: Colors.black54,
                        fontFamily: 'ProximaNova'),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        elevation: 0,
                      ),
                      onPressed: () {
                        DialogResponse _dialogResponse =
                            DialogResponse(confirmed: false);
                        completer(_dialogResponse);
                      },
                      child: Text(
                        'NO',
                        style:
                            TextStyle(color: ColorConfig.grey, fontSize: 18.0),
                      ),
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: ColorConfig.grey, elevation: 0),
                        onPressed: () async {
                          DialogResponse _dialogResponse =
                              DialogResponse(confirmed: true);
                          completer(_dialogResponse);
                        },
                        child: Text(
                          'YES',
                          style: TextStyle(color: Colors.white, fontSize: 18.0),
                        )),
                    const SizedBox(
                      width: 17,
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ConfirmationDialog extends StatelessWidget {
  final DialogRequest request;
  final Function(DialogResponse) completer;

  ConfirmationDialog({required this.request, required this.completer});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        child: Padding(
          padding: EdgeInsets.only(left: 13, top: 13, right: 21, bottom: 15),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 10.0, top: 15.0, bottom: 15.0),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.delete, color: ColorConfig.grey),
                      const SizedBox(
                        width: 8,
                      ),
                      Text(
                        'Delete',
                        style: TextStyle(
                          fontSize: 21,
                          color: ColorConfig.grey,
                          fontFamily: 'ProximaNova',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: Text(
                    'Are you sure that you want to \nRemove.',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black54,
                        fontFamily: 'ProximaNova'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        elevation: 0,
                      ),
                      onPressed: () {
                        DialogResponse _dialogResponse =
                            DialogResponse(confirmed: false);
                        completer(_dialogResponse);
                      },
                      child: Text(
                        'NO',
                        style:
                            TextStyle(color: ColorConfig.grey, fontSize: 18.0),
                      ),
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: ColorConfig.grey,
                          elevation: 0,
                        ),
                        onPressed: () async {
                          DialogResponse _dialogResponse =
                              DialogResponse(confirmed: true);
                          completer(_dialogResponse);
                        },
                        child: Text(
                          'YES',
                          style: TextStyle(color: Colors.white, fontSize: 18.0),
                        )),
                    const SizedBox(
                      width: 17,
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CustomPlanDialog extends StatelessWidget {
  final DialogRequest request;
  final Function(DialogResponse) completer;

  CustomPlanDialog({required this.request, required this.completer});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        child: Padding(
          padding: EdgeInsets.only(left: 13, top: 13, right: 21, bottom: 15),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 10.0, top: 15.0, bottom: 15.0),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.delete, color: ColorConfig.grey),
                      const SizedBox(
                        width: 8,
                      ),
                      Text(
                        'Delete',
                        style: TextStyle(
                          fontSize: 21,
                          color: ColorConfig.grey,
                          fontFamily: 'ProximaNova',
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: Text(
                    'Are you sure that you want to \nRemove.',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black54,
                        fontFamily: 'ProximaNova'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        elevation: 0,
                      ),
                      onPressed: () {
                        DialogResponse _dialogResponse =
                            DialogResponse(confirmed: false);
                        completer(_dialogResponse);
                      },
                      child: Text(
                        'NO',
                        style:
                            TextStyle(color: ColorConfig.grey, fontSize: 18.0),
                      ),
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: ColorConfig.grey,
                        elevation: 0,
                      ),
                      onPressed: () async {
                        DialogResponse _dialogResponse =
                            DialogResponse(confirmed: true);
                        completer(_dialogResponse);
                      },
                      child: Text(
                        'YES',
                        style: TextStyle(color: Colors.white, fontSize: 18.0),
                      ),
                    ),
                    const SizedBox(
                      width: 17,
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
