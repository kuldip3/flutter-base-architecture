import 'package:flutter/material.dart';

class ColorConfig {
  static Color primaryColor = Colors.white;
  static Color accentColor = Colors.orange.shade700;
  static Color lightAccentColor = Colors.orange.shade50;
  static Color boldTextColor = Colors.grey.shade800;

  static Color primaryTextColor = Colors.grey.shade700;
  static Color secondaryTextColor = Colors.grey.shade500;
  static Color green = Colors.green.shade600;

  static Color white = Colors.white;
  static Color red = Colors.red.shade500;
  static Color grey = Colors.grey.shade700;
  static Color darkGrey = Colors.grey.shade900;
  static Color lightGrey = const Color(0xFFF5F5F5);
  static Color black = Colors.black;
}
