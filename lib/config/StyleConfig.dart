import 'package:flutter/material.dart';

import 'ColorConfig.dart';

class StyleConfig {
  static TextStyle h1(Color color) {
    return TextStyle(color: color, fontSize: 20, fontWeight: FontWeight.w700);
  }

  static TextStyle h2(Color color) {
    return TextStyle(color: color, fontSize: 18, fontWeight: FontWeight.w700);
  }

  static TextStyle h3(Color color) {
    return TextStyle(color: color, fontSize: 14, fontWeight: FontWeight.w500);
  }

  static TextStyle h4(Color color) {
    return TextStyle(color: color, fontSize: 10, fontWeight: FontWeight.w400);
  }

  static TextStyle light(Color color) {
    return TextStyle(color: color, fontSize: 10, fontWeight: FontWeight.w200);
  }

  static InputDecoration getInputDecoration(
      String label, Widget? suffixIconButton) {
    return InputDecoration(
      focusColor: ColorConfig.darkGrey,
      label: Text(
        label,
        style: TextStyle(color: ColorConfig.secondaryTextColor),
      ),
      suffixIcon: suffixIconButton,
      focusedBorder: OutlineInputBorder(
        borderRadius: const BorderRadius.all(Radius.circular(12.0)),
        borderSide: BorderSide(color: ColorConfig.darkGrey, width: 2.0),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: const BorderRadius.all(Radius.circular(12.0)),
        borderSide: BorderSide(color: ColorConfig.red, width: 2.0),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: const BorderRadius.all(Radius.circular(12.0)),
        borderSide: BorderSide(color: Colors.grey.shade300, width: 1.0),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: const BorderRadius.all(Radius.circular(12.0)),
        borderSide: BorderSide(color: ColorConfig.red, width: 2.0),
      ),
    );
  }
}
