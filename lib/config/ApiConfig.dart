import 'package:flutter_dotenv/flutter_dotenv.dart';

class ApiConfig {
  static final String APP_BASE_URL =
      (dotenv.env['APP_BASE_URL'] ?? "https://app.zebrapro.in/api/v1");

  static final String WS_BASE_URL =
      (dotenv.env['WS_BASE_URL'] ?? "https://workstation.zebrapro.in/api/v1");

  static final String S3_URL = (dotenv.env['S3_URL'] ??
      "https://zebrapro-test.s3.ap-south-1.amazonaws.com");

  static final String SECURE_IMAGE_URL = (dotenv.env['SECURE_IMAGE_URL'] ??
      "https://app.zebrapro.in/secure-image/v1?key=");
// static final String API_KEY = dotenv.env['APIKEY'] ?? "";
// static final String TEMP_TOKEN = dotenv.env['TEMP_TOKEN'] ?? "";
}
