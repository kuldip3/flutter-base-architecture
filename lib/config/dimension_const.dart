class Dimen {
  static const double heightOfFormField = 0.08;
  static const double widthOfFormField = 0.88;
  static const double heightOfSizedBoxBetweenTwoField = 12;
  static const double rightFormPadding = 16;
  static const double leftFormPadding = 16;
  static const double topFormPadding = 16;
  static const double bottomFormPadding = 16;
  static const double buttonHeight = 52;
  static const double buttonWidth = 160;
}
